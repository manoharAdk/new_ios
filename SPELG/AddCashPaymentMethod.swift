//
//  AddCashPaymentMethod.swift
//  SPELG
//
//  Created by Fairoze Banu on 15/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class AddCashPaymentMethod: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var eWalletTxtLbl: UILabel!
    @IBOutlet weak var ewalletsListView: UITableView!
    @IBOutlet weak var creditOrDebitLbl: UILabel!
    @IBOutlet weak var ewalletCardsLbl: UILabel!
     @IBOutlet weak var creditOrDebitCardView: UIView!
    @IBOutlet weak var citiCardBtn: UIButton!
    @IBOutlet weak var masterCardBtn: UIButton!
    @IBOutlet weak var visaCardBtn: UIButton!
    @IBOutlet weak var eWalletCardsView: UIView!
    @IBOutlet weak var googlePayBtn: UIButton!
    @IBOutlet weak var phonePeBtn: UIButton!
    @IBOutlet weak var paytmBtn: UIButton!
    @IBOutlet weak var VPATxtField: UITextField!
    @IBOutlet weak var proceedBtn: UIButton!
    
    let paymentLogoArray = ["paytm-512.png","mobikwik.png" ,"amazon-pay.png"]
    let linkAcntTxt = ["LINK ACCOUNT","LINK ACCOUNT","LINK ACCOUNT"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ewalletsListView.delegate = self
        ewalletsListView.dataSource = self
        ewalletsListView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentLogoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "linkCell") as! HomeViewCell
        cell.paymentMethodeLogo.image = UIImage(named: paymentLogoArray[indexPath.row])
        cell.linkAcntLbl.text = linkAcntTxt[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func citiCardTapped(_ sender: Any) {
    }
    @IBAction func masterCardTapped(_ sender: Any) {
    }
    @IBAction func visaCardTapped(_ sender: Any) {
    }
    
    @IBAction func googlePayBtnTapped(_ sender: Any) {
    }
    @IBAction func phonePeBtnTapped(_ sender: Any) {
    }
    @IBAction func paytmBtnTapped(_ sender: Any) {
    }
    @IBAction func proceedBtnTapped(_ sender: Any) {
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "CashAddedSuccessfullyAlert") as! CashAddedSuccessfullyAlert
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
}
