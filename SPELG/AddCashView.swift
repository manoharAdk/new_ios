//
//  AddCashView.swift
//  SPELG
//
//  Created by Fairoze Banu on 15/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class AddCashView: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var walletTotalTxtLbl: UILabel!
    @IBOutlet weak var walletTotalView: UIView!
    @IBOutlet weak var walletTotalAmntLbl: UILabel!
    @IBOutlet weak var addCashView: UIView!
    @IBOutlet weak var addCashLbl: UILabel!
    @IBOutlet weak var cashBtn1: UIButton!
    @IBOutlet weak var cashBtn2: UIButton!
    @IBOutlet weak var cashBtn3: UIButton!
    @IBOutlet weak var enterAmntTF: UITextField!
    @IBOutlet weak var addCashBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    func configureUI(){
        cashBtn1.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cashBtn2.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cashBtn3.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        walletTotalView.layer.cornerRadius = 7
        addCashView.layer.cornerRadius = 7
        addCashBtn.layer.cornerRadius = 18
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addCashBtnTapped(_ sender: Any) {
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "AddCashPaymentMethod") as! AddCashPaymentMethod
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func cashBtn1Tapped(_ sender: Any) {
        let btn1 = "100"
        enterAmntTF.text = btn1
    }
    @IBAction func cashBtn2Tapped(_ sender: Any) {
        let btn2 = "200"
        enterAmntTF.text = btn2
    }
    @IBAction func cashBtn3Tapped(_ sender: Any) {
        let btn3 = "300"
        enterAmntTF.text = btn3
    }
    

}
