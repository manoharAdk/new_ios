//
//  AppDelegate.swift
//  SPELG
//
//  Created by Fairoze Banu on 16/03/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


struct APIKeysObject { // Super Star Super Fan
    
    static var APIKey : String!

    static var deviceID : String!
    
    static var deviceOS : String!
    
    static var deviceName : String!
    
    static var spelgVersion : String!
    
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    let tagForCustomLoader : Int = 1216
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Get the Device details

        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let deviceOS = UIDevice.current.systemVersion
        let deviceName = UIDevice.current.systemName
        let spelgVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        APIKeysObject.deviceID = deviceID
        APIKeysObject.deviceOS = deviceOS
        APIKeysObject.deviceName = deviceName
        APIKeysObject.spelgVersion = spelgVersion

        USER_DEFAULTS.set(deviceID, forKey: "deviceID")
        USER_DEFAULTS.set(deviceOS, forKey: "deviceOS")
        USER_DEFAULTS.set(deviceName, forKey: "deviceName")
        USER_DEFAULTS.set(spelgVersion, forKey: "spelgVersion")

        IQKeyboardManager.shared.enable = true
        if let key = USER_DEFAULTS.object(forKey: "APIKey") as? String {
            
            if key == "" {
                
                getPlayerInformationHandlerMethod()
            }else{
                
            }
        }
        
        

        UIApplication.shared.windows.forEach { window in
            if #available(iOS 13.0, *)
            {
                window.overrideUserInterfaceStyle = .light
            } else

            {
                // Fallback on earlier versions
            }
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


   
    
    
    // MARK:- Service Handling methods
    
    func getPlayerInformationHandlerMethod() {
        
        let params = ["DeviceID": APIKeysObject.deviceID, "DeviceName":APIKeysObject.deviceName, "AppVersion":APIKeysObject.spelgVersion, "DeviceType":1] as [String : Any]
        
        let urlString = SpelgUrlPaths.sharedInstance.getUrlPath("Splash_Screen")
        
        SpelgAPI.sharedInstance.SplegService_post(paramsDict: params as NSDictionary, urlPath:urlString,onCompletion: {
            (response,error) -> Void in
            if let networkError = error {

                if (networkError.code == -1009) {
                    print("No Internet \(String(describing: error))")

                   // AlertSingletanClass.sharedInstance.validationAlert(title: "No Internet", message: "\(error)", preferredStyle: UIAlertController.Style.alert, okLabel: "OK", targetViewController: self, okHandler:  { (action) -> Void in
                   // })
                    
                    return
                }
            }
            
            if response == nil
            {

                return
            }else
            {
                let dict = dict_responce(dict: response)
                
                if status_Check(dict: dict) {
                    
                    if let dataDict = dict.value(forKey: "extras") as? NSDictionary {
                        
                        if let data = dataDict.value(forKey: "Data") as? NSDictionary {
                            
                            if let key = data.value(forKey: "ApiKey") as? String {

                                print("APIKey is ==\(key)")
                                APIKeysObject.APIKey = key
                                USER_DEFAULTS.set(key, forKey: "APIKey")
                            }
                        }
                    }
                    
                }else
                {
                    if let errorDict = dict.value(forKeyPath: "extras") as? NSDictionary {
                        
                        if let error_Msg = errorDict.value(forKeyPath: "msg") as? String {

                            
                    }
                }
            }
        }
    })
    }
   
    }

