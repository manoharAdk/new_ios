//
//  CashAddedSuccessfullyAlert.swift
//  SPELG
//
//  Created by Fairoze Banu on 15/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class CashAddedSuccessfullyAlert: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var successfullImgView: UIImageView!
    @IBOutlet weak var paymentSuccessfullLbl: UILabel!
    @IBOutlet weak var okayBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertView.layer.cornerRadius = 10
        okayBtn.layer.cornerRadius = 18

        
    }
    @IBAction func okayBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

}
