//
//  ConfirmationAddCashAlert.swift
//  SPELG
//
//  Created by Fairoze Banu on 27/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class ConfirmationAddCashAlert: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var confirmationLbl: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var walletBalTxtLbl: UILabel!
    @IBOutlet weak var walletBalLbl: UILabel!
    
    @IBOutlet weak var entryFeeTxtLbl: UILabel!
    @IBOutlet weak var entryFeeLbl: UILabel!
    @IBOutlet weak var usableCashTxtLbl: UILabel!
    @IBOutlet weak var usableCashLbl: UILabel!
    @IBOutlet weak var moneyToBeAddedTxtLbl: UILabel!
    @IBOutlet weak var moneyToBeAdded: UILabel!
    @IBOutlet weak var addCashTxtField: UITextField!
    @IBOutlet weak var addCashBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

       alertView.layer.cornerRadius = 10
       addCashBtn.layer.cornerRadius = 15
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addCashBtnTapped(_ sender: Any) {
    }
    

}
