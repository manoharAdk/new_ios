//
//  LeaderBoardCell.swift
//  SPELG
//
//  Created by Fairoze Banu on 09/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class LeaderBoardCell: UITableViewCell {
    
    @IBOutlet weak var leaderBoardView: UIView!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var refferalID: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var expandArrowImg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
