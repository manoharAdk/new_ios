//
//  ContestView.swift
//  SPELG
//
//  Created by Fairoze Banu on 09/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class ContestView: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
       @IBOutlet weak var topHeaderView: UIView!
        @IBOutlet weak var backButton: UIButton!
        @IBOutlet weak var contestTextLbl: UILabel!
        @IBOutlet weak var walletImgView: UIImageView!
        @IBOutlet weak var timeDisplayLbl: UILabel!
        @IBOutlet weak var buttonSegmentView: UIView!
        @IBOutlet weak var prizeLeaderListView: UITableView!
        @IBOutlet weak var prizeSelView: UIView!
        @IBOutlet weak var leaderboardSelView: UIView!
        @IBOutlet weak var prizePoolView: UIView!
        @IBOutlet weak var timeBackGroundView: UIView!
        @IBOutlet weak var timeLeftLbl: UILabel!
        @IBOutlet weak var prizePoolLbl: UILabel!
        @IBOutlet weak var entryLabel: UILabel!
        @IBOutlet weak var entryFeeLbl: UILabel!
        @IBOutlet weak var rupeeSymbolLbl: UILabel!
        @IBOutlet weak var prizePoolAmountLbl: UILabel!
        @IBOutlet weak var progressViewBar: UIProgressView!
        @IBOutlet weak var spotsLeftLbl: UILabel!
        @IBOutlet weak var spotsCountLbl: UILabel!
        @IBOutlet weak var cashIconImageView: UIImageView!
        @IBOutlet weak var cashlabel: UILabel!
        @IBOutlet weak var trophyIconImg: UIImageView!
        @IBOutlet weak var percentageLbl: UILabel!
        @IBOutlet weak var ticketImg: UIImageView!
        @IBOutlet weak var noOfEntriesLbl: UILabel!

        private var selectedIndex: Int = 1
        
        override func viewDidLoad() {
            super.viewDidLoad()
            configureUI()
            
        }
        
        func configureUI(){
            
            prizeLeaderListView.delegate = self
            prizeLeaderListView.dataSource = self
            prizePoolView.layer.cornerRadius = 10
            timeBackGroundView.layer.cornerRadius = 30
            entryFeeLbl.layer.cornerRadius = 6
            entryFeeLbl.layer.masksToBounds = true
        }
        
        @IBAction func backBtnTapped(_ sender: Any) {
            dismiss(animated: true, completion: nil)
        }
        
        @IBAction func didTapButtonInSegmentView(_ sender: UIButton) {
            selectedIndex = sender.tag
            prizeSelView.isHidden = selectedIndex != 1
            leaderboardSelView.isHidden = selectedIndex != 2
            prizeLeaderListView.reloadData()
        }

        private func reloadData() {

        }

        func numberOfSections(in tableView: UITableView) -> Int {
    
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if selectedIndex == 1 {
                return 10
            } else {
                return 22
            }
          }
          
          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if selectedIndex == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! PrizeBreakUpCell
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! LeaderBoardCell
                cell.profileImgView.layer.cornerRadius = cell.profileImgView.frame.size.width/2
                return cell
            }
          }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if selectedIndex == 1{
                return 40
            }
            else{
                return 50
            }
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            if selectedIndex == 1 {
                let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HomeViewCell
                    headerCell.headerCellLbl.text = "Rank"
                return headerCell

            } else {
                let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HomeViewCell
                     headerCell.headerCellLbl.text = "ALLTeams(87)"
                return headerCell

            }
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 35
        }

}
