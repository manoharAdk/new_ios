//
//  EnterContestCodeAlertView.swift
//  SPELG
//
//  Created by Fairoze Banu on 16/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class EnterContestCodeAlertView: UIViewController {
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var privateContestLbl: UILabel!
    @IBOutlet weak var handsImageView: UIImageView!
    @IBOutlet weak var enterContestCodeTF: UITextField!
    @IBOutlet weak var joinButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        alertView.layer.cornerRadius = 10
        joinButton.layer.cornerRadius = 15
    }
    
    @IBAction func joinButtonTapped(_ sender: Any) {
        
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "MainGameDashBoard") as! MainGameDashBoard
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
}
