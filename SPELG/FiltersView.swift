//
//  FiltersView.swift
//  SPELG
//
//  Created by Fairoze Banu on 07/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class FiltersView: UIViewController {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var clearAllButton: UIButton!
      @IBOutlet weak var filtersLabel: UILabel!
    @IBOutlet weak var entryLabel: UILabel!
  //entry
    @IBOutlet weak var entryBtn1to50: UIButton!
    @IBOutlet weak var entryBtn51to1000: UIButton!
    @IBOutlet weak var entryBtn101to5000: UIButton!
    @IBOutlet weak var entryBtn5000andAbove: UIButton!
    //number Of Teams
    @IBOutlet weak var numberOfTeamsLbl: UILabel!
    @IBOutlet weak var noOfTeamsBtn2: UIButton!
    @IBOutlet weak var noOfTeamsBtn3to10: UIButton!
    @IBOutlet weak var noOfTeamsBtn11to100: UIButton!
    @IBOutlet weak var noOfTeamsBtn101to1000: UIButton!
    @IBOutlet weak var noOfTeamsBtn1000andAbove: UIButton!
    
    //Prize Pool
    @IBOutlet weak var prizePoolLabel: UILabel!
    @IBOutlet weak var prizePoolBtn1to10000: UIButton!
    @IBOutlet weak var prizePoolBtn10000to1Lakh: UIButton!
    @IBOutlet weak var prizePoolBtn1Lakhto10Lakh: UIButton!
    @IBOutlet weak var prizePoolBtn10Lakhto25Lakh: UIButton!
    @IBOutlet weak var prizePoolBtn25LakhandAbove: UIButton!
    

    @IBOutlet weak var applyButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        entryBtn1to50.layer.cornerRadius = 7
        entryBtn51to1000.layer.cornerRadius = 7
        entryBtn101to5000.layer.cornerRadius = 7
        entryBtn5000andAbove.layer.cornerRadius = 7
        noOfTeamsBtn2.layer.cornerRadius = 7
        noOfTeamsBtn3to10.layer.cornerRadius = 7
        noOfTeamsBtn11to100.layer.cornerRadius = 7
        noOfTeamsBtn101to1000.layer.cornerRadius = 7
        noOfTeamsBtn1000andAbove.layer.cornerRadius = 7
        prizePoolBtn1to10000.layer.cornerRadius = 7
        prizePoolBtn10000to1Lakh.layer.cornerRadius = 7
        prizePoolBtn1Lakhto10Lakh.layer.cornerRadius = 7
        prizePoolBtn10Lakhto25Lakh.layer.cornerRadius = 7
        prizePoolBtn25LakhandAbove.layer.cornerRadius = 7
        applyButton.layer.cornerRadius = 24
    }
    

    @IBAction func closeBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearAllBtnTapped(_ sender: Any) {
    }
    @IBAction func entryBtn1to50Tapped(_ sender: Any) {
    }
    @IBAction func entryBtn51to1000Tapped(_ sender: Any) {
    }
    @IBAction func entryBtn101to5000Tapped(_ sender: Any) {
    }
    @IBAction func entryBtn5000andAboveTapped(_ sender: Any) {
    }
    //Number Of Teams
    @IBAction func noOfTeamsBtn2Tapped(_ sender: Any) {
    }
    @IBAction func noOfTeamsBtn3to10Tapped(_ sender: Any) {
    }
    @IBAction func noOfTeamsBtn11to100Tapped(_ sender: Any) {
    }
    @IBAction func noOfTeamsBtn101to1000Tapped(_ sender: Any) {
    }
    @IBAction func noOfTeamsBtn1000andAboveTapped(_ sender: Any) {
    }
    
    //Prize Pool
    @IBAction func prizePoolBtn1to10000Tapped(_ sender: Any) {
    }
    @IBAction func prizePoolBtn10000to1LakhTapped(_ sender: Any) {
    }
    @IBAction func prizePoolBtn1Lakhto10Lakh(_ sender: Any) {
    }
    @IBAction func prizePoolBtn10Lakhto25Lakh(_ sender: Any) {
    }
    @IBAction func prizePoolBtn25LakhandAbove(_ sender: Any) {
    }
    
}
