//
//  ForgotPasswordView.swift
//  SPELG
//
//  Created by Fairoze Banu on 10/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class ForgotPasswordView: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var mobileNumOrNewPswdView: UIView!
    @IBOutlet weak var mobileOrLockImg: UIImageView!
    @IBOutlet weak var mobileOrNewPswdTF: UITextField!
    @IBOutlet weak var newPaswdEyeImg: UIImageView!
    @IBOutlet weak var newPswdEyeBtn: UIButton!
    @IBOutlet weak var confirmPswdView: UIView!
    @IBOutlet weak var confirmPswdLockImg: UIImageView!
    @IBOutlet weak var confirmPswdTF: UITextField!
    @IBOutlet weak var confirmPswdEyeImg: UIImageView!
    @IBOutlet weak var confirmPswdBtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
    
    @IBOutlet weak var passwordStackView: UIStackView!
    enum ScreenType {
        case forgotPassword
        case resetPassword
    }
    
    private let forgotPswdText = "Forgot Password "
    private let resetPswdText = "Reset Password"
    private let submitText = "Done"
    
    private var currentScreen: ScreenType = .forgotPassword
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        hideKeyboardWhenTappedAround()

    }
  //Keyboard Dissmiss function
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
                view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
        
   //UI configuration
    private func configureUI(){
        let selectedColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        let attributes = [
            NSAttributedString.Key.foregroundColor: selectedColor,
            NSAttributedString.Key.font : UIFont(name: "poppins-Medium", size: 13)!
        ]
        
        mobileNumOrNewPswdView.layer.borderWidth = 4
        mobileNumOrNewPswdView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        mobileNumOrNewPswdView.layer.cornerRadius = 25
        confirmPswdView.layer.borderWidth = 4
        confirmPswdView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        confirmPswdView.layer.cornerRadius = 25
        sendBtn.layer.cornerRadius = 20
        
        if currentScreen == .forgotPassword {
            confirmPswdView.isHidden = true
            subtitleLabel.isHidden = false
            titleLabel.text = forgotPswdText
            mobileOrLockImg.image = UIImage(named: "mobileIcon")
            mobileOrNewPswdTF.attributedPlaceholder = NSAttributedString(string: "Mobile Number", attributes:attributes)
            mobileOrNewPswdTF.keyboardType = .phonePad
            mobileOrNewPswdTF.isSecureTextEntry = false
            newPaswdEyeImg.isHidden  = true
            newPswdEyeBtn.isHidden = true
        } else{
            titleLabel.text = resetPswdText
            subtitleLabel.isHidden = true
            passwordStackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 100).isActive = true
            mobileOrLockImg.image = UIImage(named: "Lock")
            mobileOrNewPswdTF.attributedPlaceholder = NSAttributedString(string: "New Password", attributes:attributes)
            mobileOrNewPswdTF.keyboardType = .default
            confirmPswdTF.attributedPlaceholder = NSAttributedString(string: "Confirm Password", attributes:attributes)
            mobileOrNewPswdTF.text = ""
            mobileOrNewPswdTF.isSecureTextEntry = true
            sendBtn.setTitle(submitText, for: .normal)
            newPaswdEyeImg.isHidden = false
            newPswdEyeBtn.isHidden = false
            confirmPswdView.isHidden = false
            
        }
    }
    
    @IBAction func newPswdEyeBtnTapped(_ sender: Any) {
        if (mobileOrNewPswdTF.isSecureTextEntry == true) {
                self.mobileOrNewPswdTF.isSecureTextEntry = false;
            }else{
        
                self.mobileOrNewPswdTF.isSecureTextEntry = true;
            }
    }
    
    @IBAction func confirmPswdEyeBtnTapped(_ sender: Any) {
        if (confirmPswdTF.isSecureTextEntry == true) {
                self.confirmPswdTF.isSecureTextEntry = false;
            }else{
        
                self.confirmPswdTF.isSecureTextEntry = true;
            }
    }
    
    @IBAction func sendBtnTapped(_ sender: Any) {
        
        if currentScreen == .forgotPassword{
            currentScreen = .resetPassword
            
            //self.forgotPaswordHandlerMethod()
        }
        else {
            
            let nextViewController = storyboard!.instantiateViewController(withIdentifier: "PasswordChangeSuccessfulAlert") as! PasswordChangeSuccessfulAlert
            self.present(nextViewController, animated:true, completion:nil)
                    
        }
        configureUI()
    }
    
    
    func forgotPaswordHandlerMethod() {
                       
        let params = ["ApiKey": APIKeysObject.APIKey, "LoginID":self.mobileOrNewPswdTF.text!] as [String : Any]
        
        let urlString = SpelgUrlPaths.sharedInstance.getUrlPath("Forgot_User_Password")
        
        SpelgAPI.sharedInstance.SplegService_post(paramsDict: params as NSDictionary, urlPath:urlString,onCompletion: {
            (response,error) -> Void in
            if let networkError = error {

                if (networkError.code == -1009) {
                    print("No Internet \(String(describing: error))")

                   // AlertSingletanClass.sharedInstance.validationAlert(title: "No Internet", message: "\(error)", preferredStyle: UIAlertController.Style.alert, okLabel: "OK", targetViewController: self, okHandler:  { (action) -> Void in
                   // })
                    
                    return
                }
            }
            
            if response == nil
            {

                return
            }else
            {
                let dict = dict_responce(dict: response)
                
                if status_Check(dict: dict) {
                    
                    
                    if let dataDict = dict.value(forKey: "extras") as? NSDictionary {
                        
                        if let statusStr = dataDict.value(forKey: "Status") as? String {
                            
                            self.showToast(message: statusStr)


                        }
                    }
                    
                    let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "OTPVerificationView") as! OTPVerificationView
                            nextViewController.modalPresentationStyle = .fullScreen
                            self.present(nextViewController, animated:true, completion:nil)
                    
                }else
                {
                    if let errorDict = dict.value(forKeyPath: "extras") as? NSDictionary {
                        
                        if let error_Msg = errorDict.value(forKeyPath: "msg") as? String {

                            
                    }
                }
            }
        }
    })
    }
    
    
    func restePaswordHandlerMethod() {
                       
        let params = ["ApiKey": APIKeysObject.APIKey, "LoginID":self.mobileOrNewPswdTF.text!] as [String : Any]
        
        let urlString = SpelgUrlPaths.sharedInstance.getUrlPath("Reset_User_Password")
        
        SpelgAPI.sharedInstance.SplegService_post(paramsDict: params as NSDictionary, urlPath:urlString,onCompletion: {
            (response,error) -> Void in
            if let networkError = error {

                if (networkError.code == -1009) {
                    print("No Internet \(String(describing: error))")

                   // AlertSingletanClass.sharedInstance.validationAlert(title: "No Internet", message: "\(error)", preferredStyle: UIAlertController.Style.alert, okLabel: "OK", targetViewController: self, okHandler:  { (action) -> Void in
                   // })
                    
                    return
                }
            }
            
            if response == nil
            {

                return
            }else
            {
                let dict = dict_responce(dict: response)
                
                if status_Check(dict: dict) {
                    
                    
                    if let dataDict = dict.value(forKey: "extras") as? NSDictionary {
                        
                        if let statusStr = dataDict.value(forKey: "Status") as? String {
                            
                            self.showToast(message: statusStr)


                        }
                    }
                    
                    let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "OTPVerificationView") as! OTPVerificationView
                            nextViewController.modalPresentationStyle = .fullScreen
                            self.present(nextViewController, animated:true, completion:nil)
                    
                }else
                {
                    if let errorDict = dict.value(forKeyPath: "extras") as? NSDictionary {
                        
                        if let error_Msg = errorDict.value(forKeyPath: "msg") as? String {

                            
                    }
                }
            }
        }
    })
    }
}
