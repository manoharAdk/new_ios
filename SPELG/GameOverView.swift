//
//  GameOverView.swift
//  SPELG
//
//  Created by Fairoze Banu on 13/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class GameOverView: UIViewController {

    @IBOutlet weak var gameOverAlertView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var gameOverLbl: UILabel!
    @IBOutlet weak var youScoredLbl: UILabel!
    @IBOutlet weak var toBeScoredLbl: UILabel!
    @IBOutlet weak var youScoredPointsLbl: UILabel!
    @IBOutlet weak var toBeScoredPointsLbl: UILabel!
    @IBOutlet weak var viewScoreCardBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gameOverAlertView.layer.cornerRadius = 16
        viewScoreCardBtn.layer.cornerRadius = 18

    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    @IBAction func viewScoreCardBtnTapped(_ sender: Any) {
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "ScoreBoardView") as! ScoreBoardView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
}
