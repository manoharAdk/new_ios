//
//  HomeView.swift
//  SPELG
//
//  Created by Fairoze Banu on 26/03/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class HomeView: UIViewController,UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var contestTableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var timingButtonsView: UIView!
    @IBOutlet weak var timingSegment: UISegmentedControl!
    @IBOutlet weak var enterContestBtn: UIButton!
    @IBOutlet weak var createContestBtn: UIButton!
    @IBOutlet weak var sortByLabel: UILabel!
    @IBOutlet weak var allFiltersButton: UIButton!
    @IBOutlet weak var entryFeeBtn: UIButton!
    @IBOutlet weak var contestSizeBtn: UIButton!

    
    var sectionText = ["Mega Contest", "Hot Contest", "Head - to - Head"]
    var subtitle = ["Get Ready for mega Winnings!", "Filling Fast. Join Now!", "The Ultimate Face Off!"]
    
    
    var presentedVC: SideMenuView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
     
    }
    
    func configureUI(){
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "TimeLeftAlertView") as! TimeLeftAlertView
        self.present(nextViewController, animated:true, completion:nil)
        
        contestTableView.delegate = self
        contestTableView.dataSource = self
        contestTableView.reloadData()
        enterContestBtn.layer.cornerRadius = 7
        createContestBtn.layer.cornerRadius = 7
        entryFeeBtn.layer.cornerRadius = 4
        contestSizeBtn.layer.cornerRadius = 4
        timingButtonsView.layer.cornerRadius = 20
        let normalTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        timingSegment.setTitleTextAttributes(normalTitleTextAttributes, for: .normal)
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        timingSegment.setTitleTextAttributes(titleTextAttributes, for: .selected)
        timingSegment.backgroundColor = .clear
        timingSegment.layer.cornerRadius = 10
        timingSegment.layer.borderWidth = 2
        timingSegment.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
    
        let tabBar = self.tabBarController!.tabBar
        tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: UIColor.white, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height: tabBar.frame.height), lineWidth: 2.0)
        
    }
    
    @IBAction func allFiltersBtnTapped(_ sender: Any) {
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "FiltersView") as! FiltersView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
       }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! HomeViewCell
        cell2.prizePoolView.layer.cornerRadius = 7
        cell2.timeBackGroundView.layer.cornerRadius = 30
        cell2.entryFeeLbl.layer.cornerRadius = 6
        cell2.entryFeeLbl.layer.masksToBounds = true
        return cell2
    }
        
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! HomeViewCell
        headerCell.ContestTextLbl.text = sectionText[section]
        headerCell.getReadyLbl.text = subtitle[section]
      return headerCell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! HomeViewCell
        
        if section == 0{
            cell.viewMoreBtn.addTarget(self, action: #selector(viewMoreBtnTapped(sender: )), for: UIControl.Event.touchUpInside)

        }
        if section == 1{
            cell.viewMoreBtn.addTarget(self, action: #selector(viewMoreHotContBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        }
        if section == 2{
            cell.viewMoreBtn.addTarget(self, action: #selector(viewMoreHeadBtnTapped(sender: )), for: UIControl.Event.touchUpInside)
        }
        return cell

    }
    
    @objc func viewMoreBtnTapped(sender: UIButton) {
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "ViewMoreContestView") as! ViewMoreContestView
        nextViewController.contestName = "Mega Contest"
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
        print("viewMore")
    }
    
    @objc func viewMoreHotContBtnTapped(sender: UIButton) {
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "ViewMoreContestView") as! ViewMoreContestView
        nextViewController.contestName = "Hot Contest"
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
        print("viewMore")
    }
    
    @objc func viewMoreHeadBtnTapped(sender: UIButton) {
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "ViewMoreContestView") as! ViewMoreContestView
        nextViewController.contestName = "Head to Head Contest"
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
        print("viewMore")
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.backgroundColor = #colorLiteral(red: 0.9795071483, green: 0.144191891, blue: 0, alpha: 1)
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "ContestView") as! ContestView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
        print("deidselect")
          
      }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 45
    }
    
    @IBAction func enterContestCodeBtnTapped(_ sender: Any) {
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "EnterContestCodeAlertView") as! EnterContestCodeAlertView
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func createContestBtnTapped(_ sender: Any) {
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "PrivateContest") as! PrivateContest
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func enterFeeBtnTapped(_ sender: Any) {
        
    }
    
    @IBAction func contestSize(_ sender: Any) {
        
    }
    @IBAction func menuButtonTapped(_ sender: Any) {
    
            let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: "SideMenuView") as! SideMenuView
            let transition = CATransition()
            transition.duration = 1.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            view.window?.layer.add(transition, forKey: kCATransition)
            presentedVC.modalPresentationStyle = .fullScreen
            present(presentedVC, animated: false, completion: nil)
    }

    
}
extension UIViewController {
    
    open func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, pushing: Bool, completion: (() -> Void)? = nil) {
        
        if pushing {
            
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            view.window?.layer.add(transition, forKey: kCATransition)
            viewControllerToPresent.modalPresentationStyle = .fullScreen
            self.present(viewControllerToPresent, animated: false, completion: completion)
            
        } else {
            self.present(viewControllerToPresent, animated: flag, completion: completion)
        }
        
    }
    
}
