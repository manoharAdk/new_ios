//
//  HomeViewCell.swift
//  SPELG
//
//  Created by Fairoze Banu on 30/03/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class HomeViewCell: UITableViewCell {
    
    @IBOutlet weak var prizePoolView: UIView!
    @IBOutlet weak var timeBackGroundView: UIView!
    @IBOutlet weak var ContestTextLbl: UILabel!
    @IBOutlet weak var getReadyLbl: UILabel!
    @IBOutlet weak var viewMoreBtn: UIButton!
    @IBOutlet weak var timeLeftLbl: UILabel!
    @IBOutlet weak var prizePoolLbl: UILabel!
    @IBOutlet weak var entryLabel: UILabel!
    @IBOutlet weak var entryFeeLbl: UILabel!
    @IBOutlet weak var rupeeSymbolLbl: UILabel!
    @IBOutlet weak var prizePoolAmountLbl: UILabel!
    @IBOutlet weak var progressViewBar: UIProgressView!
    @IBOutlet weak var spotsLeftLbl: UILabel!
    @IBOutlet weak var spotsCountLbl: UILabel!
    @IBOutlet weak var cashIconImageView: UIImageView!
    @IBOutlet weak var cashlabel: UILabel!
    @IBOutlet weak var trophyIconImg: UIImageView!
    @IBOutlet weak var percentageLbl: UILabel!
    @IBOutlet weak var ticketImg: UIImageView!
    @IBOutlet weak var noOfEntriesLbl: UILabel!

    
    //wallet cell

    @IBOutlet weak var redeemAmountImage: UIImageView!
    @IBOutlet weak var redeemTxtLabel: UILabel!
    @IBOutlet weak var redeemAmntLbl: UILabel!
     @IBOutlet weak var addCashBtn: UIButton!
    
    //contest View
    
    @IBOutlet weak var headerCellLbl: UILabel!
    @IBOutlet weak var headerCellLeftLbl: UILabel!
    
    
    //profile side view
    
    @IBOutlet weak var iconImgView: UIImageView!
    @IBOutlet weak var textDisplayLbl: UILabel!
    
   // user profile View
    
    @IBOutlet weak var wordTextLbl: UILabel!
    @IBOutlet weak var scoreLbl: UILabel!
    
    //GameCell
    @IBOutlet weak var wordBasketTxtLbl: UILabel!
    
    //score Card view
    //word cell
    @IBOutlet weak var wordsCompletedLbl: UILabel!
    @IBOutlet weak var primaryPointsLbl: UILabel!
    @IBOutlet weak var bonusPointsLbl: UILabel!
    @IBOutlet weak var superBonusPointsLbl: UILabel!
    @IBOutlet weak var totalPointsLbl: UILabel!
    
    //score cell
    
    @IBOutlet weak var totalTxtLbl: UILabel!
    @IBOutlet weak var primaryTotalScoreLbl: UILabel!
    @IBOutlet weak var bonusPointsTotalLbl: UILabel!
    @IBOutlet weak var superBonusTotalPoints: UILabel!
    
    @IBOutlet weak var totalScoreLbl: UILabel!
    
    //more view
    
    @IBOutlet weak var moreIconsImgView: UIImageView!
    @IBOutlet weak var itemsLabel: UILabel!
    
    // add Cash view link cell
    @IBOutlet weak var paymentMethodeLogo: UIImageView!
    @IBOutlet weak var linkAcntLbl: UILabel!
    
    //transaction history
    //headerCell
    @IBOutlet weak var dateOfTransactionLbl: UILabel!
    
    //history cell
    @IBOutlet weak var withdrewCashView: UIView!
    @IBOutlet weak var rupeeLabel: UILabel!
    @IBOutlet weak var transAmntLbl: UILabel!
    @IBOutlet weak var withdrewCashLbl: UILabel!
    @IBOutlet weak var dropDownImg: UIImageView!
    
    @IBOutlet weak var historyDetailsView: UIView!
    
    @IBOutlet weak var transIDTxtLbl: UILabel!
    @IBOutlet weak var teamNameTxtLbl: UILabel!
    
    @IBOutlet weak var transIDLbl: UILabel!
    @IBOutlet weak var teamNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
