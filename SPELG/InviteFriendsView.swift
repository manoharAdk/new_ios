//
//  InviteFriendsView.swift
//  SPELG
//
//  Created by Fairoze Banu on 23/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class InviteFriendsView: UIViewController {

    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLeftLbl: UILabel!
    @IBOutlet weak var subtitleLbl: UILabel!
    @IBOutlet weak var codeView: UIView!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var copyBtn: UIButton!
    @IBOutlet weak var copyImg: UIImageView!
    @IBOutlet weak var watsupShareView: UIView!
    @IBOutlet weak var moreOptionView: UIView!
    @IBOutlet weak var watsupIcon: UIImageView!
    @IBOutlet weak var watsupLabel: UILabel!
    @IBOutlet weak var watsupButton: UIButton!
    @IBOutlet weak var moreOptionIcon: UIImageView!
    @IBOutlet weak var moreLabel: UILabel!
    @IBOutlet weak var moreOptionBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        
    }
    
    func configureUI(){
        
        copyImg.isHidden = true
        copyBtn.setTitle("copy", for: .normal)
        codeView.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
        codeView.layer.borderWidth = 2
        codeView.layer.cornerRadius = 7
        
        watsupShareView.layer.cornerRadius = 7
        watsupShareView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        watsupShareView.layer.borderWidth = 2
        
        moreOptionView.layer.cornerRadius = 7
        moreOptionView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        moreOptionView.layer.borderWidth = 2
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func copyBtnTapped(_ sender: Any) {
        
        copyBtn.setTitle("Copied", for: .normal)
        copyImg.isHidden = false
    }
    
    @IBAction func WatsupBtnTapped(_ sender: Any) {
        
    }
    
    @IBAction func moreOtptionBtnTapped(_ sender: Any) {
    }
    

}
