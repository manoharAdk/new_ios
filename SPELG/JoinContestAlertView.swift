//
//  JoinContestAlertView.swift
//  SPELG
//
//  Created by Fairoze Banu on 27/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class JoinContestAlertView: UIViewController {
    
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var confirmationLbl: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var walletBalTxtLbl: UILabel!
    @IBOutlet weak var walletBalLbl: UILabel!
    
    @IBOutlet weak var entryFeeTxtLbl: UILabel!
    @IBOutlet weak var entryFeeLbl: UILabel!
    @IBOutlet weak var usableCashTxtLbl: UILabel!
    @IBOutlet weak var usableCashLbl: UILabel!
    
    @IBOutlet weak var toPayTxtLbl: UILabel!
    @IBOutlet weak var toPayLbl: UILabel!
    @IBOutlet weak var joinContestBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        alertView.layer.cornerRadius = 10
        joinContestBtn.layer.cornerRadius = 15
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func joinContestBtnTapped(_ sender: Any) {
    }
    
    
}
