//
//  LeaderBoardView.swift
//  SPELG
//
//  Created by Fairoze Banu on 16/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class LeaderBoardView: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet weak var leaderBoardLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var leaderBoardTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        leaderBoardTableView.delegate = self
        leaderBoardTableView.dataSource = self
        leaderBoardTableView.reloadData()
        leaderBoardLbl.text = "Leaderboard"
        
        
    }

    @IBAction func backBtnTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 22
      }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! LeaderBoardCell
        cell.leaderBoardView.layer.cornerRadius = 7
        cell.profileImgView.layer.cornerRadius = cell.profileImgView.frame.size.width/2
        return cell
      }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
            return 76
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HomeViewCell
                 headerCell.headerCellLbl.text = "ALLTeams(87)"
                 headerCell.headerCellLeftLbl.text = "POINTS"
            return headerCell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "WinnersInfoView") as! WinnersInfoView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }

}
