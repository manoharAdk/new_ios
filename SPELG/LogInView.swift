//
//  LogInView.swift
//  SPELG
//
//  Created by Fairoze Banu on 16/03/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class LogInView: UIViewController,UITextFieldDelegate{
    
    enum ScreenType {
        case loginOrSignUp
        case referalCode
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var emailIDOrMobileView: UIView!
    @IBOutlet weak var profileIconImgView: UIImageView!
    @IBOutlet weak var mobileNumberTF: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordIconImgView: UIImageView!
    @IBOutlet weak var eyeBtn: UIButton!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var logInBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var dontHaveAcntLbl: UILabel!
    
//    private let loginOrSignUpText = "Login/SignUp"
//    private let logInText = "Login"
//
//    private var currentScreen: ScreenType = .loginOrSignUp
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
        
        
//        for family in UIFont.familyNames.sorted() {
//            let names = UIFont.fontNames(forFamilyName: family)
//            print("Family: \(family) Font names: \(names)")
//        }
    
    }
    
    //UI configuration
    private func configureUI() {
        
        hideKeyboardWhenTappedAround()
        mobileNumberTF.delegate = self
        passwordTF.delegate = self
        emailIDOrMobileView.layer.borderWidth = 4
        emailIDOrMobileView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        emailIDOrMobileView.layer.cornerRadius = 25
        mobileNumberTF.font = UIFont(name: "Poppins-SemiBold", size: 20)
        passwordView.layer.borderWidth = 4
        passwordView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        passwordView.layer.cornerRadius = 25
        passwordTF.font = UIFont(name: "Poppins-SemiBold", size: 20)
        logInBtn.layer.cornerRadius = 20
        
        let selectedColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        let attributes = [
            NSAttributedString.Key.foregroundColor: selectedColor,
            NSAttributedString.Key.font : UIFont(name: "poppins-Medium", size: 13)!
        ]
        mobileNumberTF.attributedPlaceholder = NSAttributedString(string: "Email ID / Mobile Number", attributes:attributes)
        passwordTF.attributedPlaceholder = NSAttributedString(string: "Password", attributes:attributes)
        
    }
    
    //Keyboard Dissmiss function
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func eyeBtnTapped(_ sender: Any) {
        if (passwordTF.isSecureTextEntry == true) {
            self.passwordTF.isSecureTextEntry = false;
        }else{
    
            self.passwordTF.isSecureTextEntry = true;
        }
        
    }
    @IBAction func logInBtnTapped(_ sender: Any) {
        
         if mobileNumberTF.text == ""{
            
            self.showToast(message: "Please Enter User ID")

            
        } else if passwordTF.text == "" {
            
            self.showToast(message: "Please Enter Password")

         }else{
            
            self.userLogInHandlerMethod()
        }
    }
    
    @IBAction func forgotPasswordBtnTapped(_ sender: Any) {
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "ForgotPasswordView") as! ForgotPasswordView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    @IBAction func signUpBtnTapped(_ sender: Any) {
        
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "SignUpView") as! SignUpView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
   
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == mobileNumberTF{
            emailIDOrMobileView.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
//            passwordView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        else if textField == passwordTF{
            passwordView.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
//            emailIDOrMobileView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == mobileNumberTF{
            emailIDOrMobileView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        else if textField == passwordTF{
            passwordView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        return true
    }
    
    
    
    func userLogInHandlerMethod() {
                       
        let params = ["ApiKey": APIKeysObject.APIKey, "LoginID":self.mobileNumberTF.text!, "Password" :self.passwordTF.text!] as [String : Any]
        
        let urlString = SpelgUrlPaths.sharedInstance.getUrlPath("User_Login")
        
        SpelgAPI.sharedInstance.SplegService_post(paramsDict: params as NSDictionary, urlPath:urlString,onCompletion: {
            (response,error) -> Void in
            if let networkError = error {

                if (networkError.code == -1009) {
                    print("No Internet \(String(describing: error))")

                   // AlertSingletanClass.sharedInstance.validationAlert(title: "No Internet", message: "\(error)", preferredStyle: UIAlertController.Style.alert, okLabel: "OK", targetViewController: self, okHandler:  { (action) -> Void in
                   // })
                    
                    return
                }
            }
            
            if response == nil
            {

                return
            }else
            {
                let dict = dict_responce(dict: response)
                
                if status_Check(dict: dict) {
                    
                    
                    if let dataDict = dict.value(forKey: "extras") as? NSDictionary {
                        
                        if let data = dataDict.value(forKey: "Data") as? NSDictionary {
                            
                            UserInformationObject.userInfoDict = data
                            USER_DEFAULTS.set(data, forKey: "User_Info_Dict")
                        }
                    }
                    
                    let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "OTPVerificationView") as! OTPVerificationView
                        nextViewController.modalPresentationStyle = .fullScreen
                        self.present(nextViewController, animated:true, completion:nil)
                    
                }else
                {
                    if let errorDict = dict.value(forKeyPath: "extras") as? NSDictionary {
                        
                        if let error_Msg = errorDict.value(forKeyPath: "msg") as? String {

                            
                    }
                }
            }
        }
    })
    }
    
}

