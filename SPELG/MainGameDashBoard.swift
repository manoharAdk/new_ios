//
//  MainGameDashBoard.swift
//  SPELG
//
//  Created by Fairoze Banu on 25/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class MainGameDashBoard: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var timeTxtLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var pointsTxtLbl: UILabel!
    @IBOutlet weak var pointsLbl: UILabel!
    @IBOutlet weak var wordBasketLbl: UILabel!
    @IBOutlet weak var wordBasketListView: UITableView!
    @IBOutlet weak var todayBonusLbl: UILabel!
    @IBOutlet weak var wordsTextField: UITextField!
    @IBOutlet weak var keyBoardBorderView: UIView!
    @IBOutlet weak var APointsLabel: UILabel!
    @IBOutlet weak var AKeyButton: UIButton!
    @IBOutlet weak var BPointsLabel: UILabel!
    @IBOutlet weak var BKeyButton: UIButton!
    @IBOutlet weak var CPointsLabel: UILabel!
    @IBOutlet weak var CKeyButton: UIButton!
    @IBOutlet weak var DPointsLabel: UILabel!
    @IBOutlet weak var DKeyButton: UIButton!
    @IBOutlet weak var EPointsLabel: UILabel!
    @IBOutlet weak var EKeyButton: UIButton!
    @IBOutlet weak var FPointsLabel: UILabel!
    @IBOutlet weak var FKeyButton: UIButton!
    @IBOutlet weak var GPointsLabel: UILabel!
    @IBOutlet weak var GKeyButton: UIButton!
    @IBOutlet weak var HPointsLabel: UILabel!
    @IBOutlet weak var HKeyButton: UIButton!
    @IBOutlet weak var IPointsLabel: UILabel!
    @IBOutlet weak var IKeyButton: UIButton!
    @IBOutlet weak var JPointsLabel: UILabel!
    @IBOutlet weak var JKeyButton: UIButton!
    @IBOutlet weak var KPointsLabel: UILabel!
    @IBOutlet weak var KKeyButton: UIButton!
    @IBOutlet weak var LPointsLabel: UILabel!
    @IBOutlet weak var LKeyButton: UIButton!
    @IBOutlet weak var MPointsLabel: UILabel!
    @IBOutlet weak var MKeyButton: UIButton!
    @IBOutlet weak var NPointsLabel: UILabel!
    @IBOutlet weak var NKeyButton: UIButton!
    @IBOutlet weak var OPointsLabel: UILabel!
    @IBOutlet weak var OKeyButton: UIButton!
    @IBOutlet weak var PPointsLabel: UILabel!
    @IBOutlet weak var PKeyButton: UIButton!
    @IBOutlet weak var QPointsLabel: UILabel!
    @IBOutlet weak var QKeyButton: UIButton!
    @IBOutlet weak var RPointsLabel: UILabel!
    @IBOutlet weak var RKeyButton: UIButton!
    @IBOutlet weak var SPointsLabel: UILabel!
    @IBOutlet weak var SKeyButton: UIButton!
    @IBOutlet weak var TPointsLabel: UILabel!
    @IBOutlet weak var TKeyButton: UIButton!
    @IBOutlet weak var UPointsLabel: UILabel!
    @IBOutlet weak var UKeyButton: UIButton!
    @IBOutlet weak var VPointsLabel: UILabel!
    @IBOutlet weak var VKeyButton: UIButton!
    @IBOutlet weak var WPointsLabel: UILabel!
    @IBOutlet weak var WKeyButton: UIButton!
    @IBOutlet weak var XPointsLabel: UILabel!
    @IBOutlet weak var XKeyButton: UIButton!
    @IBOutlet weak var YPointsLabel: UILabel!
    @IBOutlet weak var YKeyButton: UIButton!
    @IBOutlet weak var ZPointsLabel: UILabel!
    @IBOutlet weak var ZKeyButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var AKeyView: UIView!
    @IBOutlet weak var BKeyView: UIView!
    @IBOutlet weak var CKeyView: UIView!
    @IBOutlet weak var DKeyView: UIView!
    @IBOutlet weak var EKeyView: UIView!
    @IBOutlet weak var FKeyView: UIView!
    @IBOutlet weak var GKeyView: UIView!
    @IBOutlet weak var HKeyView: UIView!
    @IBOutlet weak var IKeyView: UIView!
    @IBOutlet weak var JKeyView: UIView!
    @IBOutlet weak var KKeyView: UIView!
    @IBOutlet weak var LKeyView: UIView!
    @IBOutlet weak var MKeyView: UIView!
    @IBOutlet weak var NKeyView: UIView!
    @IBOutlet weak var OKeyView: UIView!
    @IBOutlet weak var PKeyView: UIView!
    @IBOutlet weak var QKeyView: UIView!
    @IBOutlet weak var RKeyView: UIView!
    @IBOutlet weak var SKeyView: UIView!
    @IBOutlet weak var TKeyView: UIView!
    @IBOutlet weak var UKeyView: UIView!
    @IBOutlet weak var VKeyView: UIView!
    @IBOutlet weak var WKeyView: UIView!
    @IBOutlet weak var XKeyView: UIView!
    @IBOutlet weak var YKeyView: UIView!
    @IBOutlet weak var ZKeyView: UIView!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var okView: UIView!
    
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var wordBasketView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       configureUI()

    }
    
    func configureUI(){
        wordBasketListView.delegate = self
        wordBasketListView.dataSource = self
        wordBasketListView.reloadData()
        
        timeView.layer.cornerRadius = 7
        wordBasketView.layer.cornerRadius = 7
        timeTxtLbl.roundCorners([.topLeft, .topRight], radius: 7)
        pointsLbl.roundCorners([.bottomLeft, .bottomRight], radius: 7)
//        timeTxtLbl.layer.cornerRadius = 7
//        pointsLbl.layer.cornerRadius = 7
        timeTxtLbl.layer.masksToBounds = true
        pointsLbl.layer.masksToBounds = true
        wordBasketLbl.roundCorners([.topLeft, .topRight], radius: 7)
        wordBasketListView.roundCorners([.bottomLeft, .bottomRight] , radius: 7)
        APointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        APointsLabel.layer.masksToBounds = true
        AKeyView.layer.cornerRadius = 7
        BPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        BKeyView.layer.cornerRadius = 7
        CPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        CKeyView.layer.cornerRadius = 7
        DPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        DKeyView.layer.cornerRadius = 7
        EPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        EKeyView.layer.cornerRadius = 7
        FPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        FKeyView.layer.cornerRadius = 7
        GPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        GKeyView.layer.cornerRadius = 7
        HPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        HKeyView.layer.cornerRadius = 7
        IPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        IKeyView.layer.cornerRadius = 7
        JPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        JKeyView.layer.cornerRadius = 7
        KPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        KKeyView.layer.cornerRadius = 7
        LPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        LKeyView.layer.cornerRadius = 7
        MPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        MKeyView.layer.cornerRadius = 7
        NPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        NKeyView.layer.cornerRadius = 7
        OPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        OKeyView.layer.cornerRadius = 7
        PPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        PKeyView.layer.cornerRadius = 7
        QPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        QKeyView.layer.cornerRadius = 7
        RPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        RKeyView.layer.cornerRadius = 7
        SPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        SKeyView.layer.cornerRadius = 7
        TPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        TKeyView.layer.cornerRadius = 7
        UPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        UKeyView.layer.cornerRadius = 7
        VPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        VKeyView.layer.cornerRadius = 7
        WPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        WKeyView.layer.cornerRadius = 7
        XPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        XKeyView.layer.cornerRadius = 7
        YPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        YKeyView.layer.cornerRadius = 7
        ZPointsLabel.roundCorners([.topLeft, .topRight] , radius: 7)
        ZKeyView.layer.cornerRadius = 7
        deleteView.layer.cornerRadius = 7
        deleteButton.layer.cornerRadius = 7
        okButton.layer.cornerRadius = 7
        okView.layer.cornerRadius = 7
        keyBoardBorderView.layer.borderWidth = 2
        keyBoardBorderView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        keyBoardBorderView.layer.cornerRadius = 7
        wordsTextField.layer.borderWidth = 5
        wordsTextField.layer.borderColor = #colorLiteral(red: 0.1294117647, green: 0.6588235294, blue: 0, alpha: 1)
        wordsTextField.layer.cornerRadius = 7
        
        submitButton.layer.cornerRadius = 20
    
    }
    
    
    @IBAction func keyButtonTapped(_ sender: UIButton) {
        let buttonTitle = sender.currentTitle!
        print(buttonTitle)
        
    }
    @IBAction func deleteKeyTapped(_ sender: Any) {
        
    }
    @IBAction func okKeyTapped(_ sender: Any) {
        
    }
    
    @IBAction func submitBtnTapped(_ sender: Any) {
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "GameOverView") as! GameOverView
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCell", for: indexPath) as! HomeViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    

}
extension UIView {

    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         self.layer.mask = mask
    }

}
