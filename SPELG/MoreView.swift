//
//  MoreView.swift
//  SPELG
//
//  Created by Fairoze Banu on 26/03/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class MoreView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableViewList: UITableView!
    
    var imagesArray = ["inviteFriendsIcon" ,"pointSystemIcon","transactionHistoryIcon","howToPlayIcon","helpDeskIcon","aboutUsIcon","legalityIcon","terms&conditionsIcon"]
    var titlesArray = ["INVITE FRIENDS","POINT SYSTEM","TRANSACTION HISTORY","HOW TO PLAY","HELP DESK","ABOUT US","LEGALITY","TERMS AND CONDITIONS"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableViewList.delegate = self
        tableViewList.dataSource = self
        backBtn.isHidden = true
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "moreCell") as! HomeViewCell
        cell.itemsLabel.text = titlesArray[indexPath.row]
        cell.moreIconsImgView.image = UIImage(named: imagesArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0{
            let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "InviteFriendsView") as! InviteFriendsView
            nextViewController.modalPresentationStyle = .fullScreen
            self.present(nextViewController, animated:true, completion:nil)
        }
        if indexPath.row == 1{
            
        }
        if indexPath.row == 2{
            let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "TransactionHistoryView") as! TransactionHistoryView
            nextViewController.modalPresentationStyle = .fullScreen
            self.present(nextViewController, animated:true, completion:nil)
        }
        if indexPath.row == 3{
            
        }
        if indexPath.row == 4{
            
        }
        if indexPath.row == 5{
            
        }
        if indexPath.row == 6{
            
        }
        if indexPath.row == 7{
            
        }
    }

}
