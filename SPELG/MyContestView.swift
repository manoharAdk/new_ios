//
//  MyContestView.swift
//  SPELG
//
//  Created by Fairoze Banu on 26/03/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class MyContestView: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var myContestLbl: UILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var myContestTeamView: UIView!
    @IBOutlet weak var leaderBoardHeader: UIView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var leaderBoardBtn: UIButton!
    @IBOutlet weak var contestListView: UITableView!
    @IBOutlet weak var mainView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        print("mycontest")
        
    }
    func configureUI(){
        contestListView.delegate = self
        contestListView.dataSource = self

        myContestTeamView.layer.cornerRadius = 10
        myContestTeamView.isHidden = true
        leaderBoardHeader.layer.cornerRadius = 10
        leaderBoardBtn.layer.cornerRadius = 10
        segmentControl.layer.cornerRadius = 7
        segmentControl.backgroundColor = .white
        segmentControl.tintColor = .white
        segmentControl.selectedSegmentTintColor = .clear
        print("configure")
    
        let normalTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.gray]
        segmentControl.setTitleTextAttributes(normalTitleTextAttributes, for: .normal)
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        segmentControl.setTitleTextAttributes(titleTextAttributes, for: .selected)
        
        segmentControl.addTarget(self, action: #selector(segmentSelected(_:)), for: UIControl.Event.valueChanged)
        segmentControl.selectedSegmentIndex = 1
        
    }
    
    @objc func segmentSelected(_ sender: UISegmentedControl){
        segmentControl.backgroundColor = .white
        segmentControl.selectedSegmentTintColor = .clear
        let selectedIndex = self.segmentControl.selectedSegmentIndex
        switch selectedIndex
        {
        case 0:
            myContestTeamView.isHidden = true
            print("case 0")
        case 1:
            myContestTeamView.isHidden = true
            print("case 1")
        default:
            myContestTeamView.isHidden = false
            print("case 2")
        }
        contestListView.reloadData()
    }
    
    @IBAction func leaderBoardBtnTapped(_ sender: Any) {
        
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "LeaderBoardView") as! LeaderBoardView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
        print("viewMore")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let selectedIndex = self.segmentControl.selectedSegmentIndex
        switch selectedIndex
        {
        case 0:
            return 10
        case 1:
            return 5
        case 2:
            return 2
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let selectedIndex = self.segmentControl.selectedSegmentIndex
        switch selectedIndex
        {
        case 0:
            
            print("1")
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HomeViewCell
            cell.prizePoolView.layer.cornerRadius = 7
            cell.timeBackGroundView.layer.cornerRadius = 30
            cell.timeBackGroundView.backgroundColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
            cell.timeLeftLbl.text = "3h 16m left"
            cell.timeLeftLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.entryFeeLbl.layer.cornerRadius = 6
            cell.entryFeeLbl.layer.masksToBounds = true
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HomeViewCell
            cell.prizePoolView.layer.cornerRadius = 7
            cell.timeBackGroundView.layer.cornerRadius = 30
            cell.timeBackGroundView.backgroundColor = #colorLiteral(red: 1, green: 0.8274509804, blue: 0.8274509804, alpha: 1)
            cell.timeLeftLbl.text = ".LIVE"
            cell.timeLeftLbl.textColor = #colorLiteral(red: 0.6941176471, green: 0.07058823529, blue: 0.1490196078, alpha: 1)
            cell.entryFeeLbl.layer.cornerRadius = 6
            cell.entryFeeLbl.layer.masksToBounds = true
            return cell
        case 2:
             let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HomeViewCell
             cell.prizePoolView.layer.cornerRadius = 7
             cell.timeBackGroundView.layer.cornerRadius = 30
             cell.timeBackGroundView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1012414384)
             cell.timeLeftLbl.text = "Completed"
             cell.timeLeftLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0.5019607843, alpha: 1)
             cell.entryFeeLbl.layer.cornerRadius = 6
             cell.entryFeeLbl.layer.masksToBounds = true
             
            //            contestListView.translatesAutoresizingMaskIntoConstraints = false
            //            contestListView.heightAnchor.constraint(equalToConstant: 135).isActive = true
            //            myContestTeamView.translatesAutoresizingMaskIntoConstraints = false
            //            myContestTeamView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: 100).isActive = true
             return cell
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
}
