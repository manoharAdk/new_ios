//
//  OTPVerificationView.swift
//  SPELG
//
//  Created by Fairoze Banu on 17/03/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit


class OTPVerificationView: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var OTPTextField1: UITextField!
    @IBOutlet weak var OTPTextField2: UITextField!
    @IBOutlet weak var OTPTextField3: UITextField!
    @IBOutlet weak var OTPTextField4: UITextField!
    @IBOutlet weak var verifyBtn: UIButton!
    
    @IBOutlet weak var resendOTP: UIButton!
    
    @IBOutlet weak var backBtn: UIButton!
    private let OTPTitle = "Enter OTP"
    private let OTPNotification = "We have sent you access code via SMS for Mobile Verification"
    private let submitTitle = "Welcome Back!"
    private let didntGetOTPText = "Didn't get OTP?"
    private let resendTitle = "Resend"
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    
    func configureUI(){
        
        titleLabel.text = OTPTitle
        verifyBtn.layer.cornerRadius = 20
        hideKeyboardWhenTappedAround()
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func verifyBtnTapped(_ sender: Any) {
        
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "TimeSelectionView") as! TimeSelectionView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
    @IBAction func resendOTP(_ sender: Any) {
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        OTPTextField1.becomeFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // On inputing value to textfield
        if ((textField.text?.count)! <= 1  && string.count > 0){
            if(textField == OTPTextField1)
            {
                OTPTextField2.becomeFirstResponder()
            }
            if(textField == OTPTextField2)
            {
                OTPTextField3.becomeFirstResponder()
            }
            if(textField == OTPTextField3)
            {
                OTPTextField4.becomeFirstResponder()
            }
            if(textField == OTPTextField4)
            {
                OTPTextField4.resignFirstResponder()
            }

            textField.text = string
            return false
        }
        else if ((textField.text?.count)! >= 1  && string.count == 0){
            // on deleting value from Textfield
            if(textField == OTPTextField2)
            {
                OTPTextField1.becomeFirstResponder()
            }
            if(textField == OTPTextField3)
            {
                OTPTextField2.becomeFirstResponder()
            }
            if(textField == OTPTextField4)
            {
                OTPTextField3.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }
        else if ((textField.text?.count)! >= 1  )
        {
            textField.text = string
            return false
        }
        return true
    }

    
    
}

