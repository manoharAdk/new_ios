//
//  SpelgAPI.swift
//  SPELG
//
//  Created by Developer Dev on 17/07/20.
//  Copyright © 2020 vixspace. All rights reserved.
//


import Foundation
import Alamofire

typealias ServiceResponse = (NSDictionary?, NSError?) -> Void
typealias SuccessResponse = (NSDictionary?, NSInteger?) -> Void
typealias FailuerResponse = (NSString?, NSInteger?, Error?) -> Void
typealias FailureReasonse = (NSString?)-> Void

class SpelgAPI: NSObject {
   
    //1
    class var sharedInstance : SpelgAPI {
        
        //2
        struct Singleton {
            
            //3
            static let instance = SpelgAPI()
        }
        //4
        return Singleton.instance
    }
    
    //MARK: - Get Service
    func SplegService_get( paramsDict : NSDictionary, urlPath : String, onCompletion: @escaping ServiceResponse) -> Void {
        
        print("Get : ", urlPath)
                
        let params : [String : AnyObject] = paramsDict as! [String : AnyObject]
        
        AF.request(urlPath, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            // .responseString {(response)  in responseJSON
             //print(response)
            
            switch response.result {
                
            case .success(let JSON):
                onCompletion(JSON as? NSDictionary, nil)
                print("registerUserStepOne========: \(JSON)")

            case .failure(let error):
                print(error)
                onCompletion(nil, error as NSError?)
            }
        }
    }
    
    //MARK: - Post Service
    func SplegService_post( paramsDict : NSDictionary, urlPath : String, onCompletion: @escaping ServiceResponse) -> Void {
        
        print("Post : ", urlPath)
        
        print("\nParams : ", paramsDict)
                
        let params : [String : AnyObject] = paramsDict as! [String : AnyObject]
                    
            AF.request(urlPath, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{ (response) in
                // .responseString {(response)  in responseJSON
                //print(response)
                
                switch response.result {
                case .success(let JSON):
                    print("registerUserStepOne========: \(JSON)")
                    onCompletion(JSON as? NSDictionary, nil)
                    
                case .failure(let error):
                    print(error)
                    onCompletion(nil, error as NSError?)
                    
                }
                
        }
    }
    
    //MARK: - Post With Header Service
    func SplegService_post_with_header( paramsDict : NSDictionary, urlPath : String, onCompletion: @escaping ServiceResponse) -> Void {
        
        print("PostWithHeader : ", urlPath)
        
        print("\nParams : ", paramsDict)
                
        let params : [String : AnyObject] = paramsDict as! [String : AnyObject]
        
        AF.request(urlPath, method: .post, parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type": "application/json; charset=utf-8", "accept": "application/json"]).responseJSON{ (response) in
            // .responseString {(response)  in responseJSON
            //print(response)
            
            switch response.result {
            case .success(let JSON):
               print("registerUserStepOne========: \(JSON)")
                onCompletion(JSON as? NSDictionary, nil)
                
            case .failure(let error):
                print(error)
                onCompletion(nil, error as NSError?)
                
            }
            
        }
    }
    
    //MARK: - Put Service
    func SplegService_put( paramsDict : NSDictionary, urlPath : String, onCompletion: @escaping ServiceResponse) -> Void {
        
        print("Put : ", urlPath)
        
        print("\nParams : ", paramsDict)
                
        let params : [String : AnyObject] = paramsDict as! [String : AnyObject]
        
        AF.request(urlPath, method: .put, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{ (response) in
            // .responseString {(response)  in responseJSON
            //print(response)
            
            switch response.result {
            case .success(let JSON):
                 print("registerUserStepOne========: \(JSON)")
                onCompletion(JSON as? NSDictionary, nil)
                
            case .failure(let error):
                print(error)
                onCompletion(nil, error as NSError?)
            }
        }
    }
    
    //MARK: - Put With Header Service
    func SpelgService_put_with_header( paramsDict : NSDictionary, urlPath : String, onCompletion: @escaping ServiceResponse) -> Void {
        
        print("Put WithHeader : ", urlPath)
        
        print("\nParams : ", paramsDict)
                
        let params : [String : AnyObject] = paramsDict as! [String : AnyObject]
        
        AF.request(urlPath, method: .put, parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type": "application/json; charset=utf-8", "accept": "application/json"]).responseJSON{ (response) in
            // .responseString {(response)  in responseJSON
            //print(response)
            
            switch response.result {
            case .success(let JSON):
               print("registerUserStepOne========: \(JSON)")
                onCompletion(JSON as? NSDictionary, nil)
                
            case .failure(let error):
                print(error)
                onCompletion(nil, error as NSError?)
                
            }
            
        }
    }
    
    //MARK: - Put Service
    func SpelgService_transaction( paramsDict : NSDictionary, urlPath : String, onCompletion: @escaping ServiceResponse) -> Void {
        
        print("Post : ", urlPath)
        
        print("\nParams : ", paramsDict)
        
        let startDate = Date()
        
        let params : [String : AnyObject] = paramsDict as! [String : AnyObject]
        
        AF.request(urlPath, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{ (response) in
            // .responseString {(response)  in responseJSON
            //print(response)
            
//            print("Responce Time : \(urlPath) Difference = \(Date().timeIntervalSince(startDate)) Duration = \(response.timeline.totalDuration)")

            switch response.result {
                
            case .success(let JSON):
                 print("registerUserStepOne========: \(JSON)")
                onCompletion(JSON as? NSDictionary, nil)
                
            case .failure(let error):
                print(error)
                onCompletion(nil, error as NSError?)
            }
        }
    }
}
