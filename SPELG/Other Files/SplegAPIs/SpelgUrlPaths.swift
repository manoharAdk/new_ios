//
//  SpelgUrlPaths.swift
//  SPELG
//
//  Created by Developer Dev on 16/07/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import Foundation

enum Server {
    
    case local
    case dev
    case live
}

class SpelgUrlPaths {
    
    let serverType = Server.dev
    
    //1
    class var sharedInstance: SpelgUrlPaths {
        
        //2
        struct singleton {
            
            //3
            static let instance = SpelgUrlPaths()
        }
        
        //4
        return singleton.instance
    }
    
    func getUrlPath(_ section:String) -> String {
        
        var publicUrlPath = ""
        
        if serverType == .local {
            
            publicUrlPath = "https://api.spelg.com/app/"
            
        } else  if serverType == .dev {
            
            publicUrlPath = "https://api.spelg.com/app/"
            
        } else {
            
            
            publicUrlPath = "https://api.spelg.com/app/"
        }
        
        let pathToReturn = publicUrlPath + section
                return pathToReturn
    }
    
    func getMatchesUrlPath(_ section:String) -> String {
        
        var publicUrlPath = ""
        
        if serverType == .local {
            
            publicUrlPath = "http://192.168.1.4:7993/"
            
        } else  if serverType == .dev {

            publicUrlPath = "http://52.52.87.215:7993/"
            
        } else {
            
            publicUrlPath = "https://movies.quizpursuit.com/rc4/"
        }
        
        let pathToReturn = publicUrlPath + section
        
        return pathToReturn
    }
}
