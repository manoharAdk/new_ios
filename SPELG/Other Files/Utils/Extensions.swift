//
//  Extensions.swift
//  Quiz
//
//  Created by rizenew on 2/1/18.
//  Copyright © 2018 Rize. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
extension String {
    
    func deviceToken() -> String {
       /* var device_token = ""
        if let token = USER_DEFAULTS.value(forKey: DEVICE_TOKEN)
        {
            device_token = String(describing: token)
        } else
        {
            device_token = ""
        }
        */
        guard let token = USER_DEFAULTS.value(forKey: DEVICE_TOKEN) else {
            return ""
        }
        
        return token as! String
    }
    
}

extension UIDevice {
    
    var iPhoneX: Bool {
        
        if #available(iOS 11.0, tvOS 11.0, *) {
            
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        } else {
            
            return UIScreen.main.nativeBounds.height == 2436
        }
    }
}

extension UIButton{
    func setCornerRadius(cornerRadius:Int) {
        layer.cornerRadius = CGFloat(cornerRadius/2)
    }
}
//extension CALayer {
//    var borderUIColor: UIColor {
//        set {
//            self.borderColor = newValue.cgColor
//        }
//        
//        get {
//            return UIColor(cgColor: self.borderColor!)
//            @objc   }
//    }
//    
//    var shadowUIColor: UIColor {
//        set {
//            self.shadowColor = newValue.cgColor
//        }
//        
//        get {
//            return UIColor(cgColor: self.shadowColor!)
//        }
//    }
//    
//}

extension UIViewController {
    
    // Tost Message Label Programatically Creation With Animations
    func showToast(message : String) {
    
        let toastLabel = UILabel(frame: CGRect(x: 0, y: 20, width: self.view.frame.size.width, height: 50))
        toastLabel.backgroundColor = UIColor.init(named: "#27AE23")
        toastLabel.textColor = UIColor.white
        toastLabel.numberOfLines = 2
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "TrebuchetMS", size:  DEVICE_TYPE_IPAD ? 14 : 12)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 0;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 3.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
//    func hideKeyboardWhenTappedAround() {
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
//    }
//    
//    @objc func dismissKeyboard() {
//        view.frame.origin.y = 0
//        view.endEditing(true)
//        
//    }

    func displayContentController(_ content:UIViewController,compareString : String)
    {
//        self.setStatusBar(color: APP_RED_COLOR)
//        content.view.frame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.size.width, height: 50)
//        self.view.addSubview(content.view)
//
//        self.view.bringSubviewToFront(content.view)
//        content.view.isUserInteractionEnabled = true
//        content.didMove(toParent: self)
    }
    
    // Hide Controller
    func hideContentController(_ content:UIViewController) {
        
//        content.willMove(toParent: nil)
//        content.view.removeFromSuperview()
//        content.view.sendSubviewToBack(self.view)
//        content.removeFromParent()
    }
    
    func setStatusBar(color: UIColor) {
        let tag = 12321
        if let taggedView = self.view.viewWithTag(tag){
            taggedView.removeFromSuperview()
        }
        let overView = UIView()
        overView.frame = UIApplication.shared.statusBarFrame
        overView.backgroundColor = color
        overView.tag = tag
        self.view.addSubview(overView)
    }
    
    // pop To Tab Root Controller
    
    func popToTabRootController(content : UIViewController, selectedIndex : Int) {
        
        // Do something with index
        
        let rootView = self.tabBarController?.viewControllers![selectedIndex]
        
        if rootView?.isKind(of: UINavigationController.self) ?? false {
            
            let rootView_controller = rootView as! UINavigationController
            
            if selectedIndex == 3 {
                
                rootView_controller.popToRootViewController(animated: false)
                
                //NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "PopToRootViewController"), object: nil)
                
            } else {
                
                rootView_controller.popToRootViewController(animated: false)
            }
            
        }else if rootView?.isKind(of: UIViewController.self) ?? false{
            
            //            let rootView_controller = rootView
            //            rootView_controller.parentViewController(animated: false)
            // self.navigationController?.pushViewController(rootView, animated: true)
        }
    }

    
    func isValidEmail( emailString : String) ->Bool
    {
        //println("validate calendar: (testStr)")
        let emailRegEx = "[A-Z0-9a-z.%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        // "^[a-zA-Z0-9.!#$%&'+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let validMail : Bool = emailTest.evaluate(with: emailString)
        return validMail
    }
    
    func isValidPhone(phone: String) -> Bool {
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: phone)
    }
    
}


// MARK: - Extension Clasess
extension UITextField
{
    
    /**
     Set The Text Field Left Padding
     
     - parameter image Name
     
     - OutPut: cornerRadius,borderWidth,BorderColor && TextField Left Padding With Image
     */
    func setPreferences(imageName:String) {
        self.layer.cornerRadius = 20.0
        //self.layer.borderColor = UIColor(red: 222/255.0, green: 208/255.0, blue: 223/255.0, alpha: 1).cgColor
        self.layer.borderWidth = 2.0
        
        let paddingView = UIView(frame: CGRect(x:5, y: 0, width: 30, height: self.frame.height))
        let imageView = UIImageView(frame: CGRect(x:5, y:5, width:30, height:30))
        paddingView.addSubview(imageView)
        let image = UIImage(named:imageName)
        imageView.image = image
        self.leftView = paddingView
        self.leftViewMode = UITextField.ViewMode.always
        
    }
    
    func setPreferences1(imageName:String) {
        self.layer.cornerRadius = 20.0
        //self.layer.borderColor = UIColor(red: 222/255.0, green: 208/255.0, blue: 223/255.0, alpha: 1).cgColor
        self.layer.borderWidth = 2.0
        
        let paddingView = UIView(frame: CGRect(x:5, y: 0, width:20, height: self.frame.height))
        let imageView = UIImageView(frame: CGRect(x:5, y:5, width:20, height:30))
        paddingView.addSubview(imageView)
        let image = UIImage(named:imageName)
        imageView.image = image
        self.leftView = paddingView
        self.leftViewMode = UITextField.ViewMode.always
        
    }
   
    /**
     TextField Set The Curser Posison After Filling Text Field The clicking The Return Key Curser Posison Show The Next TextField Automatically
     
     - parameter TextField
     */
    class func connectFields(fields:[UITextField]) -> Void {
        guard let last = fields.last else {
            return
        }
        for i in 0 ..< fields.count - 1 {
            fields[i].returnKeyType = .next
            fields[i].addTarget(fields[i+1], action: #selector(UIResponder.becomeFirstResponder), for: .editingDidEndOnExit)
        }
        last.returnKeyType = .done
        last.addTarget(last, action: #selector(UIResponder.resignFirstResponder), for: .editingDidEndOnExit)
    }
    
    class func secondStepconnectFields(fields:[UITextField]) -> Void {
        guard let last = fields.last else {
            return
        }
        for i in 0 ..< fields.count - 1 {
            fields[i].returnKeyType = .next
            fields[i].addTarget(fields[i+1], action: #selector(UIResponder.becomeFirstResponder), for: .editingDidEndOnExit)
        }
        last.returnKeyType = .done
        last.addTarget(last, action: #selector(UIResponder.resignFirstResponder), for: .editingDidEndOnExit)
    }
}
extension UITableViewCell{
    
    func customCell(display_name: String) -> UITableViewCell {
        
    self.textLabel?.text = display_name
    self.textLabel?.textAlignment = .center
    self.textLabel?.textColor = UIColor.black
    self.textLabel?.numberOfLines = 2
    self.backgroundColor = UIColor.clear
        //CustomColor.cellColor
    self.selectionStyle = .none
    
      return self
    }
}
//extension UIView {
//    
//    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
//        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        self.layer.mask = mask
//    }
//    }
class PassThroughView: UIView {
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in subviews as [UIView] {
            if !subview.isHidden && subview.alpha > 0 && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
                return true
            }
        }
        return false
    }
}

extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}

extension UILabel {
    
    func setGradientBorderColor(colorsArray : [UIColor]) {
        
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradient.colors = colorsArray
        
        let shape = CAShapeLayer()
        shape.lineWidth = 1
        shape.path = UIBezierPath(rect: self.bounds).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        
        self.layer.addSublayer(gradient)
    }
}

extension UIApplication {

    static var appVersion: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }

    static var appBuild: String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }

    static var versionBuild: String {
        let version = appVersion, build = appBuild
        return version == build ? "v\(version)" : "v\(version)(\(build))"
    }
}
