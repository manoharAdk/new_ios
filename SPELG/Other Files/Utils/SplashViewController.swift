//
//  SplashViewController.swift
//  QuizSplashScreen
//
//  Created by rize on 30/03/18.
//  Copyright © 2018 laxman. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var splash_Image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("*******")
        
        if DEVICE_TYPE_IPHONE {
            
            // Setting Content Mode
            self.splash_Image.contentMode = .scaleToFill
        }
        else {
            
            // Setting Content Mode
            self.splash_Image.contentMode = .scaleAspectFit
        }
        
        DispatchQueue.main.async(execute: {
            self.splash_Image.loadGif(name: "splash_gifanimated")
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
