//
//  TagConstants.swift
//  Quiz
//
//  Created by rizenew on 3/2/18.
//  Copyright © 2018 Rize. All rights reserved.
//

import Foundation

struct RegisrationVCTags {
    static let email_TF = 1
    static let Confirmemail_TF = 1//2
    static let Confirmpassword_TF = 5
    static let password_TF = 6
    static let displayName_TF = 7
    static let FirstName_TF = 8
    static let LastName_TF = 9
    static let verificationCode_TF = 10
    static let Email_Image = 11
    static let UserProfile_Image = 12
    static let VerificationStatus_Image = 13
    static let ResendStatus_Image = 14
    static let SignUp_Lbl = 15
    static let Step1_Lbl = 16
    static let Step2_Lbl = 17
    static let Step3_Lbl = 18
    static let NextButton = 19
    static let Step2_NextButton = 220
    static let Alreadyhave_Lbl = 226
    static let SignINNow_Lbl = 227
    static let SubmitButton = 221
    static let ResendButton = 222
    static let OkButton = 224
    static let SuccessStatus_Lbl = 225
    static let VerificationCode_Lbl = 223
    static let info_Button_StepOne = 1216

}
struct LoginVCTags {
    static let forgotPassword_email_TF = 20
    static let enterVerification_TF = 21
    static let NewPassword_TF = 22
    static let confirmPassword_TF = 23
    static let email_TF = 24
    static let password_TF = 25
    static let loginLabel = 26
    static let rememberLabel = 27
    static let forgotLabel = 28
    static let donthaveLabel = 29
    static let signupLabel = 30
    //Forgot
    static let forgotPasswordLabel = 200
     static let submit_button = 201
    static let backToLogin_button = 202
    //Resend
    static let Re_forgotPassLbl = 203
    static let next_button = 204
    //Create New Password
    static let new_PasswordLbl = 205
    static let save_button = 206
   
}
struct GooglePlusRegistrationVCTags {
    static let displayName_TF = 30
    static let Email_Image = 31
    static let UserProfile_Image = 32
    static let step1_lbl = 33
    static let step2_lbl = 34
   static let save_button = 35
}
struct DashBoardTags {
    static let playAQuizView = 40
    static let welCome_lbl = 41
    static let staicLastLogin_lbl = 42
    static let playQuiz_lbl = 43
}
struct SelectedCategoryVCTags {
     static let Random_Btn = 140
    static let OR_Lbl = 141
    static let Recently_played_Btn = 142
    static let Popular_Btn = 143
    static let Play_Btn = 145
    
}
struct ChallengesVCTags {
    static let player_Image = 50
    static let playerNickName_lbl = 51
    static let playerName_lbl = 52
    static let challengeDate_lbl = 53
    static let challengeTopic_lbl = 54
    static let challengeremaingTime_lbl = 55
    static let accept_Btn = 56
    static let decline_Btn = 57
    static let playBtn = 58
    static let pendingBtn = 59
    static let cancelBtn = 60
    static let waitingOpponent_lbl = 61
}
struct SelectFriendVCTags {
    static let profile_image = 70
    static let name_lbl = 71
    static let challenge_button = 72
    static let friendNick_name = 73
    static let friends_Lbl = 74
    static let Add_Friend = 75
    
}
struct MessagesDetailsVCTags {
    static let messageTitel_lbl = 80
    static let messageDescription_lbl = 81
    static let messageDate_lbl = 82
    static let profile_image = 83
}
struct InboxVCTags {
    static let Staticfriends_lbl = 250
    static let more_Btn = 251
  
}
struct LeaderBoardVCTags {
    static let detailQuizPlayed_lbl = 90
    static let detailQuizAnswer_lbl = 91
    static let challenge_playerImage = 92
    static let challenge_playerRank = 93
    static let challenge_playerName = 94
    static let challenge_playerWon = 95
    static let challenge_playerWinStreak = 96
    static let Played_lbl = 97
    static let freeplay_list = 98
    static let freeplay_details = 99
    static let levels_list = 100
    static let challenge_list = 101
    
}
struct CreateProfileVCTags {
    static let DisplayName_Tf = 110
    static let Phone_Tf = 111
    static let EmailId_Tf = 112
    static let Location_Tf = 113
    static let Update_Btn = 114
    static let Firstname_Tf = 115
    static let Lasttname_Tf = 116
}
struct MoreVCTags {
    static let more_lbl = 120
}
