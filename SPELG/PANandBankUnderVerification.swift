//
//  PANandBankUnderVerification.swift
//  SPELG
//
//  Created by Fairoze Banu on 06/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class PANandBankUnderVerification: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var segmentControlView: UISegmentedControl!
    @IBOutlet weak var verificationView: UIView!
    @IBOutlet weak var verificationStatusTextLbl: UILabel!
    @IBOutlet weak var verificationConfirmationLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

              configureUI()

    }
    func configureUI(){
        
        segmentControlView.selectedSegmentIndex = 0
        segmentControlView.addTarget(self, action: #selector(segmentSelected), for: UIControl.Event.valueChanged)
        
        self.segmentControlView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.segmentControlView.layer.borderWidth = 3
        self.segmentControlView.layer.masksToBounds = true
        self.segmentControlView.layer.cornerRadius = 20.0
        self.segmentControlView.clipsToBounds = true
        
        verificationView.layer.cornerRadius = 7
        
        let normalTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
                    segmentControlView.setTitleTextAttributes(normalTitleTextAttributes, for: .normal)
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
                    segmentControlView.setTitleTextAttributes(titleTextAttributes, for: .selected)

    }
//    override func viewWillAppear(_ animated: Bool) {
//        segmentControlView.addTarget(self, action: #selector(segmentSelected), for: UIControl.Event.valueChanged)
//        self.segmentControlView.layer.masksToBounds = true
//        self.segmentControlView.layer.cornerRadius = 20.0
//    }
                
    @IBAction func backBtnTapped(_ sender: Any) {
                    
        dismiss(animated: true, completion: nil)
    }
                
               
    @objc func segmentSelected(_ sender: UISegmentedControl){

    //        segmentControlView.layer.cornerRadius = 20
    //        segmentControlView.layer.masksToBounds = true
            print("segment called")
            let selectedIndex = self.segmentControlView.selectedSegmentIndex
            switch selectedIndex
            {
            case 0:
                verificationStatusTextLbl.text = "Your PAN Details are under review"
                verificationConfirmationLbl.text = "Your PAN details has been submitted."
                print("case 0")
            default:
                verificationStatusTextLbl.text = "Your Bank Details are under review"
                verificationConfirmationLbl.text = "Your Bank details has been submitted."
                print("case 1")
            }
    }

    

}
