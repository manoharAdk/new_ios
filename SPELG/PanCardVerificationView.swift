//
//  PanCardVerificationView.swift
//  SPELG
//
//  Created by Fairoze Banu on 28/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class PanCardVerificationView: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var segmentControlView: UISegmentedControl!
    @IBOutlet weak var verificationView: UIView!
    @IBOutlet weak var panOrBankVerifyIcon: UIImageView!
    @IBOutlet weak var verifyTitleLbl: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var panNumberTF: UITextField!
    @IBOutlet weak var dateOfBirthTF: UITextField!
    @IBOutlet weak var selectStateBtn: UIButton!
    @IBOutlet weak var selectStateTF: UITextField!
    @IBOutlet weak var uploadPanOrBankBtn: UIButton!
    @IBOutlet weak var panOrPassBookImgView: UIImageView!
    @IBOutlet weak var submitAndVerifyBtn: UIButton!
    
    var imagePicker = UIImagePickerController()
    var imagePicked = 0
    
    @IBOutlet weak var selectStateView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()

    }
    func configureUI(){
        
        segmentControlView.addTarget(self, action: #selector(segmentSelected), for: UIControl.Event.valueChanged)
        
        self.segmentControlView.layer.cornerRadius = 20.0
        self.segmentControlView.layer.masksToBounds = true
        self.segmentControlView.clipsToBounds = true
        self.segmentControlView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.segmentControlView.layer.borderWidth = 3
 
        verificationView.layer.cornerRadius = 7
        nameTextField.layer.cornerRadius = 30
        nameTextField.layer.masksToBounds = true
        nameTextField.layer.borderWidth = 3
        nameTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7045965325)
        panNumberTF.layer.cornerRadius = 30
        panNumberTF.layer.masksToBounds = true
        panNumberTF.layer.borderWidth = 3
        panNumberTF.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7014661815)
        dateOfBirthTF.layer.cornerRadius = 30
        dateOfBirthTF.layer.masksToBounds = true
        dateOfBirthTF.layer.borderWidth = 3
        dateOfBirthTF.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7014661815)
        selectStateView.layer.cornerRadius = 30
        selectStateTF.layer.cornerRadius = 30
        selectStateTF.layer.masksToBounds = true
        selectStateTF.layer.borderWidth = 3
        selectStateTF.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7014661815)
        
        uploadPanOrBankBtn.layer.cornerRadius = 20
        submitAndVerifyBtn.layer.cornerRadius = 20
       
        let normalTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentControlView.setTitleTextAttributes(normalTitleTextAttributes, for: .normal)
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        segmentControlView.setTitleTextAttributes(titleTextAttributes, for: .selected)
       
        segmentControlView.selectedSegmentIndex = 0
        
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @objc func segmentSelected(_ sender: UISegmentedControl){

//        segmentControlView.layer.cornerRadius = 20
//        segmentControlView.layer.masksToBounds = true
        print("segment called")
        let selectedIndex = self.segmentControlView.selectedSegmentIndex
        switch selectedIndex
        {
        case 0:
            verifyTitleLbl.text = "VERIFY YOUR PAN"
            nameTextField.placeholder = "     Name"
            panNumberTF.placeholder = "     PAN Number"
            dateOfBirthTF.placeholder = "    Date of Birth"
            selectStateView.isHidden = false
            selectStateTF.placeholder = "    Select State"
            uploadPanOrBankBtn.setTitle("   UPLOAD PAN CARD IMAGE", for: .normal)
            panOrBankVerifyIcon.image = UIImage(named: "id-card.png")
            submitAndVerifyBtn.setTitle("SUBMIT FOR VERIFICATION", for: .normal)
            print("case 0")
        default:
            verifyTitleLbl.text = "VERIFY BANK ACCOUNT"
            nameTextField.placeholder = "    Name"
            panNumberTF.placeholder = "    Account Number"
            dateOfBirthTF.placeholder = "    IFSC Code"
            selectStateView.isHidden = true
            uploadPanOrBankBtn.setTitle("   UPLOAD PASSBOOK IMAGE", for: .normal)
            panOrBankVerifyIcon.image = UIImage(named: "bank.png")
            submitAndVerifyBtn.setTitle("SUBMIT FOR VERIFICATION", for: .normal)
            print("case 1")
        }
            
        
    }
    
//    @IBAction func segmentControlTapped(_ sender: Any) {
//    }
    
    @IBAction func selectStateBtnTapped(_ sender: Any) {
    }
    
    @IBAction func uploadPanOrBankBtnTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage{
             panOrPassBookImgView.contentMode = .scaleToFill
            panOrPassBookImgView.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    @IBAction func submitAndVerifyBtnTapped(_ sender: Any) {
        
        let selectedIndex = self.segmentControlView.selectedSegmentIndex
        switch selectedIndex
        {
        case 0:
            let nextViewController = storyboard!.instantiateViewController(withIdentifier: "PANandBankUnderVerification") as! PANandBankUnderVerification
            nextViewController.modalPresentationStyle = .fullScreen
            self.present(nextViewController, animated:true, completion:nil)
            nextViewController.segmentControlView.selectedSegmentIndex  = 0
            nextViewController.segmentControlView.layer.cornerRadius = 20
        default:
            let nextViewController = storyboard!.instantiateViewController(withIdentifier: "PANandBankUnderVerification") as! PANandBankUnderVerification
            nextViewController.modalPresentationStyle = .fullScreen
            self.present(nextViewController, animated:true, completion:nil)
            nextViewController.segmentControlView.selectedSegmentIndex  = 1
            nextViewController.verificationStatusTextLbl.text = "Your Bank Details are under review"
            nextViewController.verificationConfirmationLbl.text = "Your Bank details has been submitted."
        }
        
    }
}
