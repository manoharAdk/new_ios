//
//  PasswordChangeSuccessfulAlert.swift
//  SPELG
//
//  Created by Fairoze Banu on 11/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class PasswordChangeSuccessfulAlert: UIViewController {
    
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var successfullImgView: UIImageView!
    @IBOutlet weak var notificationLbl: UILabel!
    @IBOutlet weak var passwordChangeStatusLbl: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertView.layer.cornerRadius = 10
        loginBtn.layer.cornerRadius = 18
    }
    
    @IBAction func loginBtnTapped(_ sender: Any) {
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "TimeSelectionView") as! TimeSelectionView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
}
