//
//  PrivateContest.swift
//  SPELG
//
//  Created by Fairoze Banu on 17/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class PrivateContest: UIViewController {

    @IBOutlet weak var privateContestLbl: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var timingButtonsView: UIView!
    @IBOutlet weak var timingSegment: UISegmentedControl!
    @IBOutlet weak var enterDetailsView: UIView!
    @IBOutlet weak var contestNameLbl: UILabel!
    @IBOutlet weak var contestNameTF: UITextField!
    @IBOutlet weak var winningAmntLbl: UILabel!
    @IBOutlet weak var winningAmntTF: UITextField!
    @IBOutlet weak var contestSizeLbl: UILabel!
    @IBOutlet weak var contestCodeTF: UITextField!
    @IBOutlet weak var allowFriendsLbl: UILabel!
    @IBOutlet weak var allowFriendsSwitch: UISwitch!
    @IBOutlet weak var entryPerTeamView: UIView!
    @IBOutlet weak var entryPerTeamLbl: UILabel!
    @IBOutlet weak var teamDescriptionTV: UITextView!
    @IBOutlet weak var createContestBtn: UIButton!
    @IBOutlet weak var rupeeLbl: UILabel!
    @IBOutlet weak var entryTeamAmountLbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        

    }
    func configureUI(){
        
        enterDetailsView.layer.cornerRadius = 10
        entryPerTeamView.layer.cornerRadius = 10
        createContestBtn.layer.cornerRadius = 18
        allowFriendsSwitch.isOn = false
        entryTeamAmountLbl.isHidden = true
        rupeeLbl.isHidden = true
        timingButtonsView.layer.cornerRadius = 20
        let normalTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        timingSegment.setTitleTextAttributes(normalTitleTextAttributes, for: .normal)
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        timingSegment.setTitleTextAttributes(titleTextAttributes, for: .selected)
        timingSegment.backgroundColor = .clear
        timingSegment.layer.cornerRadius = 10
        timingSegment.layer.borderWidth = 2
        timingSegment.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
    }
    
    @IBAction func allowFriendsSwitch(_ sender: Any) {
        if allowFriendsSwitch.isOn {
            entryTeamAmountLbl.isHidden = false
            rupeeLbl.isHidden = false
            entryTeamAmountLbl.text = "20"
        } else {
            entryTeamAmountLbl.isHidden = true
            rupeeLbl.isHidden = true
        }
    }
    
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createContestBtnTapped(_ sender: Any) {
        let nextViewController = storyboard?.instantiateViewController(withIdentifier: "PrivateContestWinnersView") as! PrivateContestWinnersView
        nextViewController.modalPresentationStyle = .fullScreen
        present(nextViewController, animated: true)
        
    }
    
    

}
