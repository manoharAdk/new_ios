//
//  PrivateContestWinnersView.swift
//  SPELG
//
//  Created by Fairoze Banu on 20/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class PrivateContestWinnersView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var privateContestLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var contestSizeLbl: UILabel!
    @IBOutlet weak var prizePoolLbl: UILabel!
    @IBOutlet weak var prizeAmntLbl: UILabel!
    @IBOutlet weak var contestSizeNoLbl: UILabel!
    @IBOutlet weak var prizePoolAmntLbl: UILabel!
    @IBOutlet weak var balAmntLbl: UILabel!
    @IBOutlet weak var chooseWinnersLbl: UILabel!
    @IBOutlet weak var chooseWinnersBtn: UIButton!
    @IBOutlet weak var winnersRankListView: UITableView!
    @IBOutlet weak var createContestBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        winnersRankListView.delegate = self
        winnersRankListView.dataSource = self
        winnersRankListView.reloadData()
        createContestBtn.layer.cornerRadius = 18
        chooseWinnersBtn.layer.borderWidth = 2
        chooseWinnersBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0.5019607843, alpha: 1)

    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func chooseWinnersBtnTapped(_ sender: Any) {
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "SelectTeamSizeView") as! SelectTeamSizeView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func createContestBtnTapped(_ sender: Any) {
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 5
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HomeViewCell
            return cell
      }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
            return 27
    }
    
}
