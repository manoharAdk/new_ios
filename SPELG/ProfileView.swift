//
//  ProfileView.swift
//  SPELG
//
//  Created by Fairoze Banu on 26/03/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class ProfileView: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profiNameView: UIView!
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var awardView: UIView!
    @IBOutlet weak var totalWinningsTxtLbl: UILabel!
    @IBOutlet weak var totalWinningsLbl: UILabel!
    @IBOutlet weak var awardImgView: UIImageView!
    @IBOutlet weak var accountDetailsView: UIView!
    @IBOutlet weak var userIconImg: UIImageView!
    @IBOutlet weak var accountDetailsLbl: UILabel!
    @IBOutlet weak var walletTotalTxtLbl: UILabel!
    @IBOutlet weak var walletTotalLbl: UILabel!
    @IBOutlet weak var winningsTxtLbl: UILabel!
    @IBOutlet weak var winningsLbl: UILabel!
    @IBOutlet weak var generalStaticView: UIView!
    @IBOutlet weak var statisticIcon: UIImageView!
    @IBOutlet weak var generalStaticLbl: UILabel!
    @IBOutlet weak var contentsTxtLbl: UILabel!
    @IBOutlet weak var winsTxtLbl: UILabel!
    @IBOutlet weak var contentsLbl: UILabel!
    @IBOutlet weak var winsLbl: UILabel!
    @IBOutlet weak var personalDetailsView: UIView!
    @IBOutlet weak var personalDetailsIcon: UIImageView!
    
    @IBOutlet weak var nameTxtLbl: UILabel!
    @IBOutlet weak var emailTxtLbl: UILabel!
    @IBOutlet weak var mobileTxtLbl: UILabel!
    @IBOutlet weak var panNumTxtLbl: UILabel!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var panNumLbl: UILabel!
    
    @IBOutlet weak var editPasswordBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       configureUI()
        
    }
    func configureUI(){
        backBtn.isHidden = true
        profiNameView.layer.cornerRadius = 7
        awardView.layer.cornerRadius = 7
        accountDetailsView.layer.cornerRadius = 7
        generalStaticView.layer.cornerRadius = 7
    }
    
    @IBAction func editPasswordBtnTapped(_ sender: Any) {
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
