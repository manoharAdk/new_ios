//
//  RateUsView.swift
//  SPELG
//
//  Created by Fairoze Banu on 13/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class RateUsView: UIViewController {

    @IBOutlet weak var rateUsAlertView: UIView!
    @IBOutlet weak var rateUsTitleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var star1Btn: UIButton!
    @IBOutlet weak var star2Btn: UIButton!
    @IBOutlet weak var star3Btn: UIButton!
    @IBOutlet weak var star4Btn: UIButton!
    @IBOutlet weak var star5Btn: UIButton!
    @IBOutlet weak var notNowBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rateUsAlertView.layer.cornerRadius = 10
        notNowBtn.layer.borderWidth = 2
        notNowBtn.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
        notNowBtn.layer.cornerRadius = 18
        submitBtn.layer.cornerRadius = 18
        
    }
    
    
    @IBAction func star1BtnTapped(_ sender: Any) {
    }
    @IBAction func star2BtnTapped(_ sender: Any) {
    }
    @IBAction func star3BtnTapped(_ sender: Any) {
    }
    @IBAction func star4BtnTapped(_ sender: Any) {
    }
    @IBAction func star5BtnTapped(_ sender: Any) {
    }
    
    @IBAction func notNowBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func submitBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
