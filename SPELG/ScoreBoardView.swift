//
//  ScoreBoardView.swift
//  SPELG
//
//  Created by Fairoze Banu on 12/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class ScoreBoardView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var totalTxtLbl: UILabel!
    @IBOutlet weak var totalScoreLbl: UILabel!
    @IBOutlet weak var headerTitlesView: UIView!
    @IBOutlet weak var wordLbl: UILabel!
    
    @IBOutlet weak var primaryPointsLbl: UILabel!
    @IBOutlet weak var bonusPointsLbl: UILabel!
    @IBOutlet weak var superBonusLbl: UILabel!
    @IBOutlet weak var totalTitleLbl: UILabel!
    @IBOutlet weak var scoreTableView: UITableView!
    @IBOutlet weak var viewLeaderBoardBtn: UIButton!
    @IBOutlet weak var okBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    
    func configureUI(){
        scoreTableView.delegate = self
        scoreTableView.dataSource = self
        headerTitlesView.layer.cornerRadius = 7
        scoreTableView.layer.cornerRadius = 7
        viewLeaderBoardBtn.layer.cornerRadius = 18
        okBtn.layer.cornerRadius = 18
    }

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "wordsCell") as! HomeViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scoreCell") as! HomeViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 53
    }
    
    @IBAction func viewLeaderBoardTapped(_ sender: Any) {
        let nextViewController = storyboard?.instantiateViewController(withIdentifier: "LeaderBoardView") as! LeaderBoardView
        nextViewController.modalPresentationStyle = .fullScreen
        present(nextViewController, animated: true)
        
    }
    
    @IBAction func okBtnTapped(_ sender: Any) {
        
        self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        
    }
    

}
