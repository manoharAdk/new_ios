//
//  SelectTeamSizeView.swift
//  SPELG
//
//  Created by Fairoze Banu on 21/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class SelectTeamSizeView: UIViewController {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var selectTeamTitleLbl: UILabel!
    
    // 4 winners
    @IBOutlet weak var winners4Lbl: UILabel!
    @IBOutlet weak var winners4Btn: UIButton!
    
    @IBOutlet weak var win4Rank1Lbl: UILabel!
    @IBOutlet weak var win4Rank2Lbl: UILabel!
    @IBOutlet weak var win4Rank3Lbl: UILabel!
    @IBOutlet weak var win4Rank4Lbl: UILabel!
    
    @IBOutlet weak var win4PercentLbl1: UILabel!
    @IBOutlet weak var win4PercentLbl2: UILabel!
    @IBOutlet weak var win4PercentLbl3: UILabel!
    @IBOutlet weak var win4PercentLbl4: UILabel!
    
    @IBOutlet weak var win4PriceLbl1: UILabel!
    @IBOutlet weak var win4PriceLbl2: UILabel!
    @IBOutlet weak var win4PriceLbl3: UILabel!
    @IBOutlet weak var win4PriceLbl4: UILabel!
    
    // 3 winners
    @IBOutlet weak var winners3Lbl: UILabel!
    @IBOutlet weak var winners3Btn: UIButton!
    
    @IBOutlet weak var win3Rank1Lbl: UILabel!
    @IBOutlet weak var win3Rank2Lbl: UILabel!
    @IBOutlet weak var win3Rank3Lbl: UILabel!
    
    @IBOutlet weak var win3PercentLbl1: UILabel!
    @IBOutlet weak var win3PercentLbl2: UILabel!
    @IBOutlet weak var win3PercentLbl3: UILabel!
    
    @IBOutlet weak var win3PriceLbl1: UILabel!
    @IBOutlet weak var win3PriceLbl2: UILabel!
    @IBOutlet weak var win3PriceLbl3: UILabel!
    
    //2 winners
    @IBOutlet weak var winners2Lbl: UILabel!
    @IBOutlet weak var winners2Btn: UIButton!
    @IBOutlet weak var win2Rank1Lbl: UILabel!
    @IBOutlet weak var win2Rank2Lbl: UILabel!
    
    @IBOutlet weak var win2PercentLbl1: UILabel!
    @IBOutlet weak var win2PercentLbl2: UILabel!
    
    @IBOutlet weak var win2PriceLbl1: UILabel!
    @IBOutlet weak var win2PriceLbl2: UILabel!
    
    // 1 winner
    @IBOutlet weak var winners1Lbl: UILabel!
    @IBOutlet weak var winners1Btn: UIButton!
    @IBOutlet weak var win1Rank1Lbl: UILabel!
    @IBOutlet weak var win1PercentLbl1: UILabel!
    @IBOutlet weak var win1PriceLbl1: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func winner4BtnTapped(_ sender: Any) {
        winners4Btn.setImage(UIImage(named: "Radiobutton On .png"), for: .normal)
        winners3Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
        winners2Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
        winners1Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
    }
    
    @IBAction func winner3BtnTapped(_ sender: Any) {
        winners3Btn.setImage(UIImage(named: "Radiobutton On .png"), for: .normal)
        winners4Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
        winners2Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
        winners1Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
    }
    
    @IBAction func winner2BtnTapped(_ sender: Any) {
        winners2Btn.setImage(UIImage(named: "Radiobutton On .png"), for: .normal)
        winners4Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
        winners3Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
        winners1Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
    }
    
    @IBAction func winner1BtnTapped(_ sender: Any) {
        winners1Btn.setImage(UIImage(named: "Radiobutton On .png"), for: .normal)
        winners3Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
        winners2Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
        winners4Btn.setImage(UIImage(named: "Radiobutton Off.png"), for: .normal)
    }
    
}

