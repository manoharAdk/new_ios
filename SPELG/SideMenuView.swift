//
//  SideMenuView.swift
//  SPELG
//
//  Created by Fairoze Banu on 08/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class SideMenuView: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var mailIDLbl: UILabel!
    @IBOutlet weak var backgroundBtn: UIButton!
    @IBOutlet weak var profileDetailsView: UITableView!
    
    
    var iconsArray = ["userBlack.png","walletOpen","percentage","invite" ,"settingIcon", "share","logout"]
    var textArray = ["PROFILE","MY WALLET","MY REWARDS & OFFERS","INVITE FRIENDS","SETTINGS","SHARE APP","LOGOUT"]
    
    let imageView = UIImageView(image: UIImage(named: "user.png"))
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       configureUI()
    }
    
    func configureUI(){
        
        profileDetailsView.delegate = self
        profileDetailsView.dataSource = self
        profileDetailsView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return iconsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HomeViewCell
        cell.iconImgView.image = UIImage(named: iconsArray[indexPath.row])
        cell.textDisplayLbl.text = textArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
    
                let transition = CATransition()
                transition.duration = 1.0
                transition.type = CATransitionType.push
                transition.subtype = CATransitionSubtype.fromRight
                transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                view.window?.layer.add(transition, forKey: kCATransition)
                dismiss(animated: true, completion: nil)
            }
    
    @IBAction func backgroundBtn(_ sender: Any) {
        
        let transition = CATransition()
        transition.duration = 1.0
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window?.layer.add(transition, forKey: kCATransition)
        dismiss(animated: true, completion: nil)
    }
    
    
}
extension UIImageView {
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}
