//
//  SignUpView.swift
//  SPELG
//
//  Created by Fairoze Banu on 14/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit


struct UserInformationObject {
    
    static var userInfoDict = NSDictionary()
    
}

class SignUpView: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var nickNameView: UIView!
    @IBOutlet weak var nickNameTF: UITextField!
    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var EmailTF: UITextField!
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var pswrdEyeBtn: UIButton!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var confirmPasswordEyeBtn: UIButton!
    @IBOutlet weak var referalView: UIView!
    @IBOutlet weak var referalTF: UITextField!
    @IBOutlet weak var createBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        hideKeyboardWhenTappedAround()
    }
    
    func configureUI(){
        
        userNameTF.delegate = self
        nickNameTF.delegate = self
        EmailTF.delegate = self
        mobileTF.delegate = self
        passwordTF.delegate = self
        confirmPasswordTF.delegate = self
        referalTF.delegate = self
        
        let selectedColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        let attributes = [
            NSAttributedString.Key.foregroundColor: selectedColor,
            NSAttributedString.Key.font : UIFont(name: "poppins-Medium", size: 13)!
        ]
        userNameTF.attributedPlaceholder = NSAttributedString(string: "User Name", attributes:attributes)
        nickNameTF.attributedPlaceholder = NSAttributedString(string: "Nick Name", attributes:attributes)
        EmailTF.attributedPlaceholder = NSAttributedString(string: "Email ID", attributes:attributes)
        mobileTF.attributedPlaceholder = NSAttributedString(string: "Mobile Number", attributes:attributes)
        passwordTF.attributedPlaceholder = NSAttributedString(string: "Password", attributes:attributes)
        confirmPasswordTF.attributedPlaceholder = NSAttributedString(string: "Confirm Password", attributes:attributes)
        referalTF.attributedPlaceholder = NSAttributedString(string: "Referraal Code (Optional)", attributes:attributes)
        
        userNameView.layer.borderWidth = 3
        userNameView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        userNameView.layer.cornerRadius = 25
        nickNameView.layer.borderWidth = 3
        nickNameView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        nickNameView.layer.cornerRadius = 25
        mailView.layer.borderWidth = 3
        mailView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        mailView.layer.cornerRadius = 25
        mobileView.layer.borderWidth = 3
        mobileView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        mobileView.layer.cornerRadius = 25
        passwordView.layer.borderWidth = 3
        passwordView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        passwordView.layer.cornerRadius = 25
        confirmPasswordView.layer.borderWidth = 3
        confirmPasswordView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        confirmPasswordView.layer.cornerRadius = 25
        referalView.layer.borderWidth = 3
        referalView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        referalView.layer.cornerRadius = 25
        createBtn.layer.cornerRadius = 18
    }
    
    //Keyboard Dissmiss function
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    @IBAction func pswdEyeBtnTapped(_ sender: Any) {
        if (passwordTF.isSecureTextEntry == true) {
                self.passwordTF.isSecureTextEntry = false;
            }else{
        
                self.passwordTF.isSecureTextEntry = true;
            }
    }
    
    @IBAction func confirmPswdEyeBtnTapped(_ sender: Any) {
        if (confirmPasswordTF.isSecureTextEntry == true) {
                self.confirmPasswordTF.isSecureTextEntry = false;
            }
        else{
        
                self.confirmPasswordTF.isSecureTextEntry = true;
            }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == userNameTF{
        userNameView.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
        }
        if textField == nickNameTF{
        nickNameView.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
        }
        if textField == EmailTF{
            
            self.emailVerificationAPICall(emailStr: self.EmailTF.text!)
        mailView.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
        }
        if textField == mobileTF{
        mobileView.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
        }
        if textField == passwordTF{
        passwordView.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
        }
        if textField == confirmPasswordTF{
        confirmPasswordView.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
        }
        if textField == referalTF{
        referalView.layer.borderColor = #colorLiteral(red: 0.004859850742, green: 0.09608627111, blue: 0.5749928951, alpha: 1)
        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == userNameTF{
        userNameView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        if textField == nickNameTF{
        nickNameView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        if textField == EmailTF{
        mailView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        if textField == mobileTF{
        mobileView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        if textField == passwordTF{
        passwordView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        if textField == confirmPasswordTF{
        confirmPasswordView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        if textField == referalTF{
        referalView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        return true
    }
    
    @IBAction func createBtnTapped(_ sender: Any) {
        
        if (userNameTF.text == "" ){
            
            self.showToast(message: "Please Enter User Name")

        }else if nickNameTF.text == "" {
            
            self.showToast(message: "Please Enter Nick Name")

        }
        else if EmailTF.text == "" {
            
            self.showToast(message: "Please Enter Email Id")

        }
        else if !isValidEmail(emailString: EmailTF.text!){
                
            self.showToast(message: "Please Enter Valid Email Id")

        }
            
        else if mobileTF.text == "" {
            
            self.showToast(message: "Pleas Eenter Mobile Number")
            
        }else if !isValidPhone(phone: self.mobileTF.text!) {
            
            self.showToast(message: "Please Enter Valid Mobile Number")
        }
        
        else if passwordTF.text == ""{
            
            self.showToast(message: "Please Enter Password")

            
        } else if confirmPasswordTF.text == "" {
            
            self.showToast(message: "Please Enter Confirm Password")

        }
        else if confirmPasswordTF.text != passwordTF.text {
                   
            self.showToast(message: "Enter Password Not Matched")

        }else{
            
            self.userSignUpHandlerMethod()
        }
    }

    
    func emailVerificationAPICall(emailStr: String) {
        
        print("email is ==\(emailStr)")
    }
    
    // MARK:- Service Handling methods
    
    func userSignUpHandlerMethod() {

        let currentLocale = NSLocale.current
        let countryCode = currentLocale.regionCode
                       
        let params = ["ApiKey": APIKeysObject.APIKey, "CountryCode":getCountryCallingCode(countryRegionCode: countryCode ?? ""), "PhoneNumber":self.mobileTF.text!, "Name":self.userNameTF.text! , "Nick_Name": self.nickNameTF.text!, "EmailID" : self.EmailTF.text! , "Password" :self.passwordTF.text! , "Whether_Referral": false, "Referral_Code": "", "OTP":1024] as [String : Any]
        
        let urlString = SpelgUrlPaths.sharedInstance.getUrlPath("User_SignUp")
        
        SpelgAPI.sharedInstance.SplegService_post(paramsDict: params as NSDictionary, urlPath:urlString,onCompletion: {
            (response,error) -> Void in
            if let networkError = error {

                if (networkError.code == -1009) {
                    print("No Internet \(String(describing: error))")

                   // AlertSingletanClass.sharedInstance.validationAlert(title: "No Internet", message: "\(error)", preferredStyle: UIAlertController.Style.alert, okLabel: "OK", targetViewController: self, okHandler:  { (action) -> Void in
                   // })
                    
                    return
                }
            }
            
            if response == nil
            {

                return
            }else
            {
                let dict = dict_responce(dict: response)
                
                if status_Check(dict: dict) {
                    
//                    if let dataDict = dict.value(forKey: "extras") as? NSDictionary {
//
//                        if let data = dataDict.value(forKey: "Data") as? NSDictionary {
//
//                            UserInformationObject.userInfoDict = data
//                            USER_DEFAULTS.set(data, forKey: "User_Info_Dict")
//                        }
//                    }
//

                    let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "LogInView") as! LogInView
                    nextViewController.modalPresentationStyle = .fullScreen
                    self.present(nextViewController, animated:true, completion:nil)
                    
                }else
                {
                    if let errorDict = dict.value(forKeyPath: "extras") as? NSDictionary {
                        
                        if let error_Msg = errorDict.value(forKeyPath: "msg") as? String {

                            
                    }
                }
            }
        }
    })
    }
}
