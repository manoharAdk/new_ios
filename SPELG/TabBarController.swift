//
//  TabBarController.swift
//  SPELG
//
//  Created by Fairoze Banu on 02/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
//    var barItemImagesArray

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarController?.delegate = self
        let selectedColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        let unselectedColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6)
        
        if let items = tabBar.items {
        for item in items {
            item.setTitleTextAttributes([.foregroundColor: selectedColor], for: .selected)
            item.setTitleTextAttributes([.foregroundColor: unselectedColor], for: .normal)
            }
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if viewController is HomeView {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let controller = storyboard.instantiateViewController(withIdentifier: "storyboardID") as? HomeView {
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: true, completion: nil)
            }
            return false
        }

        // Tells the tab bar to select other view controller as normal
        return true
    }
//    func morningView(){
//        if let controller = storyboard?.instantiateViewController(withIdentifier: "storyboardID") as? HomeView{
//            controller.modalPresentationStyle = .fullScreen
//            controller.timingSegment.selectedSegmentIndex = 1
//            self.present(controller, animated: true, completion: nil)
//        }
//    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
    }
    
}
extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: size.height - lineWidth, width: size.width, height: lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
