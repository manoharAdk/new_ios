//
//  TimeLeftAlertView.swift
//  SPELG
//
//  Created by Fairoze Banu on 13/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class TimeLeftAlertView: UIViewController {

    @IBOutlet weak var timeLeftAlertView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var timeLeftLbl: UILabel!
    @IBOutlet weak var notificationLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        timeLeftAlertView.layer.cornerRadius = 10
    }

    @IBAction func closeBtnTapped(_ sender: Any) {
        
       dismiss(animated: true, completion: nil)
    }
}
