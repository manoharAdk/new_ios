//
//  TimeSelectionView.swift
//  SPELG
//
//  Created by Fairoze Banu on 25/03/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class TimeSelectionView: UIViewController,UITabBarDelegate {
    
    @IBOutlet weak var morningBGView: UIView!
    @IBOutlet weak var morningButton: UIButton!
    @IBOutlet weak var morningImageView: UIImageView!
    @IBOutlet weak var afternoonBGView: UIView!
    @IBOutlet weak var afternoonButton: UIButton!
    @IBOutlet weak var afternoonImageView: UIImageView!
    @IBOutlet weak var eveningBGView: UIView!
    @IBOutlet weak var eveningButton: UIButton!
    
    @IBOutlet weak var eveningImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ConfigureUI()
    }
    
    func ConfigureUI(){
        morningButton.layer.cornerRadius = 7
        afternoonButton.layer.cornerRadius = 7
        eveningButton.layer.cornerRadius = 7
        morningBGView.layer.cornerRadius = 7
        afternoonBGView.layer.cornerRadius = 7
        eveningBGView.layer.cornerRadius = 7
        morningImageView.roundCorners([.topRight, .bottomRight], radius: 7)
        afternoonImageView.roundCorners([.topRight, .bottomRight], radius: 7)
        eveningImageView.roundCorners([.topRight, .bottomRight], radius: 7)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "RateUsView") as! RateUsView
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func morningBtnTapped(_ sender: Any) {
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    @IBAction func afternoonBtnTapped(_ sender: Any) {
//        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
//        nextViewController.modalPresentationStyle = .fullScreen
//        nextViewController.morningView()
//        self.present(nextViewController, animated:true, completion:nil)
    }
    @IBAction func eveningBtnTapped(_ sender: Any) {
//        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "OTPVerificationView") as! OTPVerificationView
//        nextViewController.modalPresentationStyle = .fullScreen
//        self.present(nextViewController, animated:true, completion:nil)
    }
    
}
extension UIView {

    func roundcorners(_ corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         self.layer.mask = mask
    }

}
