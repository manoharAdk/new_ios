//
//  TransactionHistoryView.swift
//  SPELG
//
//  Created by Fairoze Banu on 16/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class TransactionHistoryView: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var backBtn: UIButton!

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var transactionHistoryListView: UITableView!

    var historyArray1 = [orderHistoryModel]()
    var historyArray2 = [orderHistoryModel]()
    var mainSection = [[orderHistoryModel]]()

    let section1 = ["Hello1","Hello2","Hello3","Hello4","Hello5","Hello6","Hello7"]
    let section2 = ["HI1","HI2","HI3","Hi4","Hi5","Hi6"]

    override func viewDidLoad() {

        super.viewDidLoad()
        transactionHistoryListView.delegate = self
        transactionHistoryListView.dataSource = self
//        transactionHistoryView.reloadData()

        for i in section1
        {
          let tempModel = orderHistoryModel()
            tempModel.name = i
            tempModel.isSelected = false
            historyArray1.append(tempModel)
        }

        for i in section2
        {
          let tempModel = orderHistoryModel()
                   tempModel.name = i
                   tempModel.isSelected = false
                   historyArray2.append(tempModel)
               }
        mainSection.append(historyArray1)
        mainSection.append(historyArray2)

    }

    @IBAction func backBtnTapped(_ sender: Any) {

        dismiss(animated: true, completion: nil)

    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return mainSection.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  Cell1 = tableView.dequeueReusableCell(withIdentifier: "sectionCell") as! HomeViewCell
      return Cell1

    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 37
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int

    {
        let section = mainSection[section]
        return section.count

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell2 = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as! HomeViewCell
        let section = mainSection[indexPath.section]
        let data = section[indexPath.row]
        if data.isSelected == false
        {
            cell2.heightAnchor.constraint(equalToConstant: 49).isActive = true
            cell2.dropDownImg.image =  UIImage(named: "Icon ionic-ios-arrow-dropdown-circle.png")

        }
        else {
            cell2.heightAnchor.constraint(equalToConstant: 150).isActive = true
            cell2.dropDownImg.image = UIImage(named: "Icon ionic-ios-arrow-dropdown-circleUp.png")
        }
        return cell2

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let section = mainSection[indexPath.section]
       let data = section[indexPath.row]
       if data.isSelected == false
        {
            data.isSelected  = true
        }

       else {
            data.isSelected  = false
        }
        transactionHistoryListView.reloadData()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat

    {
        let section = mainSection[indexPath.section]
        let data = section[indexPath.row]
        if data.isSelected == false
        {
            return 49
        }
        else

        {
            return 150
        }
    }

    @objc func dropDownBtnTapped()

    {

    }
    //this is model class

    class orderHistoryModel : NSObject
    {
        var name = String()
        var isSelected = false

    }

}
