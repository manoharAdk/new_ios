//
//  ViewMoreContestView.swift
//  SPELG
//
//  Created by Fairoze Banu on 07/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class ViewMoreContestView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
   
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var contestTextLabel: UILabel!
    @IBOutlet weak var contestAvailable: UILabel!
    @IBOutlet weak var walletImageView: UIImageView!
    @IBOutlet weak var allFiltersBtn: UIButton!
    @IBOutlet weak var contestListView: UITableView!
    var contestName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contestTextLabel.text = contestName
        contestListView.delegate = self
        contestListView.dataSource = self
        contestListView.reloadData()

    }
    
    
    @IBAction func allFiltersBtnTapped(_ sender: Any) {
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "FiltersView") as! FiltersView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 22
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HomeViewCell
        cell.prizePoolView.layer.cornerRadius = 7
        cell.timeBackGroundView.layer.cornerRadius = 30
        cell.entryFeeLbl.layer.cornerRadius = 6
        cell.entryFeeLbl.layer.masksToBounds = true
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (indexPath.row == 0){
            
           let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "ContestView") as! ContestView
            nextViewController.modalPresentationStyle = .fullScreen
            self.present(nextViewController, animated:true, completion:nil)
            print("deidselect")
        }
    }
    
}
