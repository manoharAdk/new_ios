//
//  WalletView.swift
//  SPELG
//
//  Created by Fairoze Banu on 26/03/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class WalletView: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var myWalletView: UIView!
    @IBOutlet weak var myWalletTxtLbl: UILabel!
    @IBOutlet weak var walletAmountLbl: UILabel!
    @IBOutlet weak var walletAmountListVC: UITableView!
    @IBOutlet weak var verifyLabel: UILabel!
    @IBOutlet weak var verifyDescriptionLbl: UILabel!
    @IBOutlet weak var verifyButton: UIButton!
    
   
    var imagesArray = ["RefferalAmount.png","winningAmount.png","DepositAmount.png"]
    var textArray = ["Referral Amount","Winning Amount","Deposit Amount"]
    var amountArray = ["50","1000","500"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()

    }
    
    func configureUI(){
        
        walletAmountListVC.delegate = self
        walletAmountListVC.dataSource = self
        walletAmountListVC.reloadData()
        myWalletView.layer.cornerRadius = 23
        walletAmountListVC.layer.cornerRadius = 23
        verifyButton.layer.cornerRadius = 12
        
    }
    
    @IBAction func verifyNowBtnTapped(_ sender: Any) {
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "PanCardVerificationView") as! PanCardVerificationView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "walletCell", for: indexPath) as! HomeViewCell
        
        cell.redeemAmountImage.image = UIImage(named: imagesArray[indexPath.row])
        cell.redeemTxtLabel.text = textArray[indexPath.row]
        cell.redeemAmntLbl.text = amountArray[indexPath.row]
        cell.addCashBtn.layer.cornerRadius = 15
        
        cell.addCashBtn.tag = indexPath.row
        cell.addCashBtn.addTarget(self, action: #selector(addCashBtnTapped), for: UIControl.Event.touchUpInside)
        
        if indexPath.row == 0{
            cell.addCashBtn.isHidden = true
        }
        else if indexPath.row == 1{
            cell.addCashBtn.isHidden = true
        }
        else if indexPath.row == 2{
            cell.addCashBtn.isHidden = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 143
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @objc func addCashBtnTapped(){
        
        let nextViewController = storyboard!.instantiateViewController(withIdentifier: "AddCashView") as! AddCashView
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
        
    }
}
