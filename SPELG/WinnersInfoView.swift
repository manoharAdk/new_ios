//
//  WinnersInfoView.swift
//  SPELG
//
//  Created by Fairoze Banu on 25/04/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class WinnersInfoView: UIViewController, UITableViewDelegate, UITableViewDataSource {

   @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var userProfileImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userRefID: UILabel!
    @IBOutlet weak var scoreView: UIView!
    @IBOutlet weak var scoredTxtLbl: UILabel!
    @IBOutlet weak var scoreLbl: UILabel!
    @IBOutlet weak var wordsAndScoreListView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        scoreView.layer.cornerRadius = 11
        userProfileImg.layer.cornerRadius = userProfileImg.frame.size.width/2
        wordsAndScoreListView.delegate = self
        wordsAndScoreListView.dataSource = self
        wordsAndScoreListView.reloadData()
    
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell") as! HomeViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }

}
