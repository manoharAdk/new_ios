//
//  WithdrawCashView.swift
//  SPELG
//
//  Created by Fairoze Banu on 04/05/20.
//  Copyright © 2020 vixspace. All rights reserved.
//

import UIKit

class WithdrawCashView: UIViewController {
    
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var balanceAmntView: UIView!
    @IBOutlet weak var walletBalanceLbl: UILabel!
    @IBOutlet weak var balanceCashLbl: UILabel!
    @IBOutlet weak var bankPaymentView: UIView!
    @IBOutlet weak var paytmView: UIView!
    @IBOutlet weak var bankLogoImgView: UIImageView!
    @IBOutlet weak var bankNameLbl: UILabel!
    @IBOutlet weak var accountNumberLbl: UILabel!
    @IBOutlet weak var bankSelectedBtn: UIButton!
    @IBOutlet weak var paytmLogoImgView: UIImageView!
    @IBOutlet weak var paytmLbl: UILabel!
    @IBOutlet weak var paytmSelectedBtn: UIButton!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var mobileNumTF: UITextField!
    @IBOutlet weak var mobileNumSeperator: UIView!
    @IBOutlet weak var enterAmntTF: UITextField!
    @IBOutlet weak var enterAmntSeperator: UIView!
    @IBOutlet weak var withdrawButton: UIButton!
    @IBOutlet weak var detailsStackView: UIStackView!
    @IBOutlet weak var mobileNumView: UIView!
    @IBOutlet weak var withdrawAmntView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    
    func configureUI(){
        balanceAmntView.layer.cornerRadius = 7
        bankPaymentView.layer.cornerRadius = 7
        paytmView.layer.cornerRadius = 7
        detailsView.layer.cornerRadius = 7
        withdrawButton.layer.cornerRadius = 15
        mobileNumView.isHidden = true
        paytmSelectedBtn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func bankBtnTapped(_ sender: Any) {
        mobileNumView.isHidden = false
        bankSelectedBtn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
        paytmSelectedBtn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        print("height of bank btn")
    }
    
    @IBAction func paytmBtnTapped(_ sender: Any) {
        
        mobileNumView.isHidden = true
        paytmSelectedBtn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
        bankSelectedBtn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
    }
    @IBAction func withdrawBtnTapped(_ sender: Any) {
    }
    
}
